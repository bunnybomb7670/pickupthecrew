﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PickUpTheCrew
{
    public class Animator : GameObject
    {
        private int _sheetSizeX;
        private int _sheetSizeY;
        private Vector2 _tileSize;

        private int _sheetLimit;
        private int _sheetIndex;

        private double _time;
        private double _curTime;
        private double _timeRate;

        private int _repeatX;
        private int _repeatY;

        /// <summary>
        /// the size (in tiles) of the sheet X
        /// </summary>
        public int sheetSizeX { get { return _sheetSizeX; } }

        /// <summary>
        /// The size (in tiles) of the sheet Y
        /// </summary>
        public int sheetSizeY { get { return _sheetSizeY; } }

        /// <summary>
        /// The current index in the sheet animation
        /// </summary>
        public int sheetIndex { get { return _sheetIndex; } }

        /// <summary>
        /// The amount of times the texture repeats X
        /// </summary>
        public int repeatX { get { return _repeatX; } }

        /// <summary>
        /// The amount of times the texture repeats Y
        /// </summary>
        public int repeatY { get { return _repeatY; } }

        /// <summary>
        /// The size of the tile
        /// </summary>
        public Vector2 tileSize { get { return _tileSize; } }

        /// <summary>
        /// The current position in the sheet as a 2D coordinate
        /// </summary>
        public Vector2 sheetPos
        {
            get
            {
                return new Vector2((int)_sheetIndex % _sheetSizeX, (int)_sheetIndex / _sheetSizeX);
            }
        }

        public Animator(MainGame game, Texture2D texture, int sheetSizeX, int sheetSizeY, int sheetLimit, double animationRate, Vector2 tileSize, int repeatX, int repeatY)
            : base(game, texture, null)
        {
            persistent = false;

            _tileSize = tileSize;
            _sheetSizeX = sheetSizeX;
            _sheetSizeY = sheetSizeY;
            _sheetLimit = sheetLimit;
            _timeRate = animationRate;
            _repeatX = repeatX;
            _repeatY = repeatY;

            _renderer = new AnimatorRenderer(this, texture, tileSize);
        }

        public override void update(GameTime gameTime)
        {
            _time += gameTime.ElapsedGameTime.TotalSeconds;

            if (_time >= _curTime)
            {
                _curTime = _time + _timeRate;
                nextFrame();
            }
            base.update(gameTime);
        }

        /// <summary>
        /// Move to the next animation frame
        /// </summary>
        private void nextFrame()
        {
            if (_sheetIndex >= _sheetLimit)
            {
                _sheetIndex = 0;
            }
            else
            {
                _sheetIndex++;
            }
        }

    }
}