﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Xml;

namespace PickUpTheCrew
{
    /// <summary>
    /// An enum to have a managed way of storing the type of crew
    /// </summary>
    public enum CrewType
    {
        Lieutenant_Sky,
        Midshipman_Verde,
        Cabinboy_Crimson,
        Stoker_Orange,
        Able_Seaman_Shocking_Pink,
        Commander_Custard
    }

    public class Crew : PhysicsObject, IPoolableObject, IAttachable
    {
        private bool _attached;
        private PhysicsObject _parent;

        private float _anchorOffset = 35.0f;
        private float _topRopeAnchorOffset = -10.0f;
        private float _bottomRopeAnchorOffset = 10.0f;
        private float _distanceThreshold = 8.5f;
        private float _distanceThresholdSquared;
        private float _pickupDistance = 50.0f;
        private float _pickupDistanceSquared;

        private CrewType _crewType;

        /// <summary>
        /// Whether or not the crew is attached to a parent object
        /// </summary>
        public bool attached { get { return _attached; } }

        /// <summary>
        /// A public reference for objects to get the type of this crew
        /// </summary>
        public CrewType crewType { get { return _crewType; } }

        public Crew(MainGame game, Texture2D texture, Shape collisionShape, CrewType crewType)
            : base(game, texture, collisionShape)
        {
            tag = "crew";
            trigger = true;
            maxVelocity = 2.5f;
            renderer.renderDepth = 0.6f;

            setCrewData(crewType);

            _pickupDistanceSquared = _pickupDistance * _pickupDistance;
            _distanceThresholdSquared = _distanceThreshold * _distanceThreshold;
        }

        public Crew(MainGame game, XmlNode node)
            : base(game, node) { }

        /// <summary>
        /// Set the data in the crew, this allows re-usability and pooling to reset data
        /// </summary>
        /// <param name="crewType"></param>
        public void setCrewData(CrewType crewType)
        {
            transform.rotation = MathHelper.ToRadians(RandomUtils.random.Next(0, 360));
            _crewType = crewType;
            _renderer.setTexture(game.renderManager.getTextureForCrew(crewType));
        }

        public override void update(GameTime gameTime)
        {
            Player player = game.gameplay.player;
            float distance = Vector2.DistanceSquared(transform.position, player.getCrewAttachmentPoint());

            if (_attached)
            {
                IAttachable a = (IAttachable)_parent;
                Vector2 anchor = a.getAnchor();

                float speed = 0.1f;

                float dist = Vector2.DistanceSquared(transform.position, anchor);

                if (dist > _distanceThresholdSquared)
                {
                    Vector2 dir = anchor - transform.position;
                    Vector2 movement = VectorUtils.truncate(dir * speed, dist < _distanceThresholdSquared ? _distanceThresholdSquared : dist);

                    transform.position += movement;

                    float ang = VectorUtils.pointTowards(transform.position, anchor);
                    transform.rotation = ang;
                }

            }
            else
            {
                if (distance <= _pickupDistanceSquared) // within range
                {
                    player.attachCrew(this);
                }
            }

            base.update(gameTime);
        }

        /// <summary>
        /// Catch the draw "event" and draw special things such as the rope and debug data
        /// </summary>
        /// <param name="batch"></param>
        public override void onDraw(SpriteBatch batch)
        {
            if (enabled)
            {
                if (game.drawDebugData && !_attached)
                {
                    Vector2 a = game.gameplay.player.getCrewAttachmentPoint();
                    Vector2 b = transform.position;

                    Vector2 dir = Vector2.Normalize(b - a);
                    Vector2 c = a + dir * _pickupDistance;

                    game.renderManager.drawLine(b, c, Color.Red, 1, 0.01f);
                    game.renderManager.drawLine(a, c, Color.Lime, 1, 0.01f);
                }
                else if(_attached) // draw rope
                {
                    IAttachable a = (IAttachable)_parent;

                    game.renderManager.drawLine(getTopAnchor(), a.getBottomAnchor(), Color.SaddleBrown, 1, 0.2f);
                }
            }
            base.onDraw(batch);
        }

        public override void saveCustomData(XmlWriter writer)
        {
            writer.WriteElementString("Attached", _attached.ToString());
            writer.WriteElementString("Parent", _parent == null ? "-1" : _parent.ID.ToString());
            writer.WriteElementString("CrewType", _crewType.ToString());

            base.saveCustomData(writer);
        }

        public override void loadCustomData(XmlNode node)
        {
            _attached = bool.Parse(node.SelectSingleNode("Attached").InnerText);
            _crewType = getCrewTypeFromString(node.SelectSingleNode("CrewType").InnerText);

            _pickupDistanceSquared = _pickupDistance * _pickupDistance;
            _distanceThresholdSquared = _distanceThreshold * _distanceThreshold;

            base.loadCustomData(node);
        }

        public override void postLoad(XmlNode node)
        {
            int i = int.Parse(node.SelectSingleNode("Parent").InnerText);

            if (i != -1)
            {
                _parent = (PhysicsObject)game.gameObjectManager.getObject(i);
            }

            base.postLoad(node);
        }

        /// <summary>
        /// Attach the crew to a parent object
        /// </summary>
        /// <param name="parent"></param>
        public void attachCrew(PhysicsObject parent)
        {
            _parent = parent;
            _attached = true;
            tag = "attachedCrew";
        }

        /// <summary>
        /// When added to the pool, reset data values
        /// </summary>
        public void onAddedToPool()
        {
            _attached = false;
            _parent = null;
            tag = "crew";
        }

        /// <summary>
        /// When released from the pool
        /// </summary>
        public void onReleasedFromPool() { }

        /// <summary>
        /// Get the string form of the crew type ( for printing to console etc..)
        /// </summary>
        /// <returns></returns>
        public string getCrewName()
        {
            switch(crewType)
            {
                case CrewType.Able_Seaman_Shocking_Pink: return "Able Seaman Shocking-Pink";
                case CrewType.Cabinboy_Crimson: return "Cabinboy Crimson";
                case CrewType.Commander_Custard: return "Commander Custard";
                case CrewType.Lieutenant_Sky: return "Lieutenant Sky";
                case CrewType.Midshipman_Verde: return "Midshipman Verde";
                case CrewType.Stoker_Orange: return "Stoker Orange";
                default: throw new Exception("Failed to get valid crew type");
            }
        }

        /// <summary>
        /// Convert a string into a CrewType instance
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public CrewType getCrewTypeFromString(string input)
        {
            switch (input)
            {
                case "Able_Seaman_Shocking_Pink": return CrewType.Able_Seaman_Shocking_Pink;
                case "Cabinboy_Crimson": return CrewType.Cabinboy_Crimson;
                case "Commander_Custard": return CrewType.Commander_Custard;
                case "Lieutenant_Sky": return CrewType.Lieutenant_Sky;
                case "Midshipman_Verde": return CrewType.Midshipman_Verde;
                case "Stoker_Orange": return CrewType.Stoker_Orange;
                default: throw new Exception("Failed to get valid crew type");
            }
        }

        /// <summary>
        /// The the default anchor
        /// </summary>
        /// <returns></returns>
        public Vector2 getAnchor()
        {
            return VectorUtils.rotateVectorAroundPointWithOffset(transform.position, new Vector2(0, _anchorOffset), transform.rotation);
        }

        /// <summary>
        /// The top anchor
        /// </summary>
        /// <returns></returns>
        public Vector2 getTopAnchor()
        {
            return VectorUtils.rotateVectorAroundPointWithOffset(transform.position, new Vector2(0, _topRopeAnchorOffset), transform.rotation);
        }

        /// <summary>
        /// The bottom anchor
        /// </summary>
        /// <returns></returns>
        public Vector2 getBottomAnchor()
        {
            return VectorUtils.rotateVectorAroundPointWithOffset(transform.position, new Vector2(0, _bottomRopeAnchorOffset), transform.rotation);
        }
    }
}
