﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PickUpTheCrew
{
    public class EdgeBoundary : PhysicsObject
    {

        public EdgeBoundary(MainGame game, Texture2D texture, Shape collisionShape)
            : base(game, texture, collisionShape) { }

        /// <summary>
        /// Catch the collision event and check for player reset states
        /// </summary>
        /// <param name="other"></param>
        public override void onCollision(PhysicsObject other)
        {
            if (other.tag == "player")
            {
                Player p = (Player)other;

                if(p.crewCollected)
                {
                    game.gameplay.nextLevel();
                }
            }

            base.onCollision(other);
        }

    }
}
