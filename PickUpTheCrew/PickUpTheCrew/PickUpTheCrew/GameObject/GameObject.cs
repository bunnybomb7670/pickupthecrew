﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Xml;

namespace PickUpTheCrew
{
    public class GameObject
    {
        private MainGame _game;
        private int _id;
        private string _tag;
        private Transform _transform;
        private bool _persistent;
        private bool _destroyOnLoad;
        private bool _enabled;

        /// <summary>
        /// Protected to allow access to child classes and allow them to manipulate the Renderer component
        /// </summary>
        protected Renderer _renderer;

        /// <summary>
        /// Game reference to which this object is part of
        /// </summary>
        public MainGame game { get { return _game; } }

        /// <summary>
        /// Transform component of the object, manages position, rotation, translation etc.
        /// </summary>
        public Transform transform { get { return _transform; } }

        /// <summary>
        /// Renderer component of the object, manages drawing etc.
        /// </summary>
        public Renderer renderer { get { return _renderer; } }

        /// <summary>
        /// Is the object "active" ingame
        /// </summary>
        public bool enabled { get { return _enabled; } set { _enabled = value; } }

        /// <summary>
        /// Does the object persist after saving/loading
        /// </summary>
        public bool persistent { get { return _persistent; } set { _persistent = value; } }

        /// <summary>
        /// Should the object be destroyed when the game re-loads
        /// </summary>
        public bool destroyOnLoad { get { return _destroyOnLoad; } set { _destroyOnLoad = value; } }

        /// <summary>
        /// The Unique ID of this object
        /// </summary>
        public int ID { get { return _id; } }

        /// <summary>
        /// A string based way of identifying an object
        /// </summary>
        public string tag { get { return _tag; } set { _tag = value; } }

        /// <summary>
        /// Default setup, everything is automatically created
        /// </summary>
        /// <param name="game"></param>
        /// <param name="texture"></param>
        public GameObject(MainGame game, Texture2D texture)
        {
            game.gameObjectManager.registerGameObject(this, out _id);
            _game = game;
            _transform = new Transform(this);
            _renderer = new Renderer(this, texture);
            _enabled = true;
            _persistent = true;
            _destroyOnLoad = true;
            _tag = "";
        }

        /// <summary>
        /// Pass in a custom renderer or NULL for constructors which initialize a new renderer
        /// AFTER base construction. Potentially unsafe if not used properly due to the possibility
        /// of the Renderer being NULL and not set.
        /// </summary>
        /// <param name="game"></param>
        /// <param name="texture"></param>
        /// <param name="customRenderer"></param>
        public GameObject(MainGame game, Texture2D texture, Renderer renderer)
        {
            game.gameObjectManager.registerGameObject(this, out _id);
            _game = game;
            _transform = new Transform(this);
            _renderer = renderer;
            _enabled = true;
            _persistent = true;
            _destroyOnLoad = true;
            _tag = "";
        }

        /// <summary>
        /// Construct a GameObject from an XmlNode, relies on a NON CORRUPTED data source otherwise there are
        /// going to be parsing errors when trying to find data that does not necessarily exist.
        /// </summary>
        /// <param name="node"></param>
        public GameObject(MainGame game, XmlNode node)
        {
            load(game, node);
        }

        /// <summary>
        /// Save the object data to an XmlWriter
        /// </summary>
        /// <param name="writer"></param>
        public void save(XmlWriter writer)
        {
            // Save default data
            writer.WriteStartElement("GameObject");

            writer.WriteElementString("Type", this.GetType().FullName);
            writer.WriteElementString("ID", _id.ToString());
            writer.WriteElementString("Enabled", _enabled.ToString());
            writer.WriteElementString("Tag", _tag);
            writer.WriteElementString("DestroyOnLoad", _destroyOnLoad.ToString());

            _transform.save(writer);
            _renderer.save(writer);

            // Save any derived object data
            saveCustomData(writer);

            writer.WriteEndElement();
        }

        /// <summary>
        /// Load the object data from an XmlNode
        /// </summary>
        /// <param name="game"></param>
        /// <param name="node"></param>
        public void load(MainGame game, XmlNode node)
        {
            _id = int.Parse(node.SelectSingleNode("ID").InnerText);
            game.gameObjectManager.registerGameObject(this, _id);

            _game = game;
            _transform = new Transform(this, node.SelectSingleNode("Transform"));
            _renderer = IOUtils.createRendererInstanceFromNode(this, node.SelectSingleNode("Renderer"));
            _persistent = true; // Automatically assume the object is persistent because otherwise, we wouldnt be loading one in.
            _destroyOnLoad = bool.Parse(node.SelectSingleNode("DestroyOnLoad").InnerText);
            _tag = node.SelectSingleNode("Tag").InnerText;
            _enabled = bool.Parse(node.SelectSingleNode("Enabled").InnerText);

            // Load any custom data from the node
            loadCustomData(node);
        }

        /// <summary>
        /// Create an instance of the renderer from an XmlNode
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public virtual Renderer getRenderer(XmlNode node)
        {
            return new Renderer(this, node);
        }

        /// <summary>
        /// Logic update, called (hopefully) every 16ms
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void update(GameTime gameTime) { }

        /// <summary>
        /// Catch the draw event for the object to allow for extra drawing
        /// </summary>
        /// <param name="batch"></param>
        public virtual void onDraw(SpriteBatch batch) { }

        /// <summary>
        /// Save any custom object data to an XmlWriter
        /// </summary>
        /// <param name="writer"></param>
        public virtual void saveCustomData(XmlWriter writer) { }

        /// <summary>
        /// Load any custom object data from an XmlWriter
        /// </summary>
        /// <param name="node"></param>
        public virtual void loadCustomData(XmlNode node) { }

        /// <summary>
        /// Called after initial loading has completed, this method can be used to reference objects without the issue of creation order
        /// </summary>
        /// <param name="node"></param>
        public virtual void postLoad(XmlNode node) { }

        /// <summary>
        /// Called when the object is about to be unloaded, to tell any dependent objects to unload themselves
        /// </summary>
        public virtual void onUnload() { }
    }
}