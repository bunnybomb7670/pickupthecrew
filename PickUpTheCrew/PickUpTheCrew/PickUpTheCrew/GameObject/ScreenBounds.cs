﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace PickUpTheCrew
{
    public class ScreenBounds
    {
        private MainGame _game;
        private EdgeBoundary[] _screenBounds;

        /// <summary>
        /// The amount in pixels the screenbounds will extend off the edge of the screen
        /// </summary>
        private int RECTANGLE_MOD = 50;


        public ScreenBounds(MainGame game)
        {
            _game = game;
            _screenBounds = new EdgeBoundary[4];

            for (int i = 0; i < 4; i++)
            {
                EdgeBoundary obj = new EdgeBoundary(game, null, new Shape(new Vector2[] { new Vector2(0, 0) }));
                obj.persistent = false;
                obj.colliderStatic = true;
                _screenBounds[i] = obj;
            }

            recalculateScreenBounds();
        }

        /// <summary>
        /// Calculate the colliders to prevent objects escaping off the edge of the screen, this is done by spawning 4 rectangles
        /// which are scaled to the X and Y edges of the screen
        /// </summary>
        public void recalculateScreenBounds()
        {
            Shape[] shapes = new Shape[4];

            Vector2 bounds = _game.windowBounds;

            float _mod = RECTANGLE_MOD / 2;
            float _xH = bounds.X / 2;
            float _yH = bounds.Y / 2;

            shapes[0] = new Shape(new Vector2[] { new Vector2(-_xH, -_mod), new Vector2(-_xH, _mod), new Vector2(_xH, _mod), new Vector2(_xH, -_mod) });
            shapes[1] = new Shape(new Vector2[] { new Vector2(-_xH, -_mod), new Vector2(-_xH, _mod), new Vector2(_xH, _mod), new Vector2(_xH, -_mod) });
            shapes[2] = new Shape(new Vector2[] { new Vector2(-_mod, -_yH), new Vector2(-_mod, _yH), new Vector2(_mod, _yH), new Vector2(_mod, -_yH) });
            shapes[3] = new Shape(new Vector2[] { new Vector2(-_mod, -_yH), new Vector2(-_mod, _yH), new Vector2(_mod, _yH), new Vector2(_mod, -_yH) });

            _screenBounds[0].transform.position = new Vector2(_xH, -_mod);
            _screenBounds[1].transform.position = new Vector2(_xH, bounds.Y + _mod);
            _screenBounds[2].transform.position = new Vector2(-_mod, _yH);
            _screenBounds[3].transform.position = new Vector2(bounds.X + _mod, _yH);

            for (int i = 0; i < 4; i++)
            {
                _screenBounds[i].shape = shapes[i];
            }
        }

        /// <summary>
        /// Get the shapes to spawn for the bounding area
        /// </summary>
        /// <returns></returns>
        private Shape[] getShapesForBounds()
        {
            Shape[] s = new Shape[4];

            Vector2 bounds = _game.windowBounds;

            float _mod = RECTANGLE_MOD / 2;
            float _xH = bounds.X / 2;
            float _yH = bounds.Y / 2;

            s[0] = new Shape(new Vector2[] { new Vector2(-_xH, -_mod), new Vector2(-_xH, _mod), new Vector2(_xH, _mod), new Vector2(_xH, -_mod) });
            s[1] = new Shape(new Vector2[] { new Vector2(-_xH, -_mod), new Vector2(-_xH, _mod), new Vector2(_xH, _mod), new Vector2(_xH, -_mod) });
            s[2] = new Shape(new Vector2[] { new Vector2(-_mod, -_yH), new Vector2(-_mod, _yH), new Vector2(_mod, _yH), new Vector2(_mod, -_yH) });
            s[3] = new Shape(new Vector2[] { new Vector2(-_mod, -_yH), new Vector2(-_mod, _yH), new Vector2(_mod, _yH), new Vector2(_mod, -_yH) });

            return s;
        }

    }
}