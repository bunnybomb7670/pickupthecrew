﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using System.Xml;

namespace PickUpTheCrew
{

    public class Shark : PhysicsObject, IPoolableObject
    {
        private bool _angry; // Whether or not the shark is angry and chasing the player
        private bool _searching; // Whether or not the shark is searching for a target

        private bool _demoShark; // Whether or not this shark is for the DEMO mode

        private float _defaultMoveSpeed = 0.8f; // The default shark speed
        private float _moveSpeed = 0.8f; // The speed at which the shark moves
        private float _randomSpeed = 0.25f; // Modifier to provide random speed bonuses per shark

        private float _aggroDistance = 200.0f; // The distance at which the player has to be within to aggrovate the shark
        private float _aggroDistanceSquared; // A squared alternative to aggro distance to prevent frequent SQRT calculations.
        private float _deAggroDistance = 350.0f; // The distance at which the shark loses intrest in the player and returns to its crew
        private float _deAggroDistanceSquared; // A squared alternative to the deAggroDistance to prevent frequent SQRT calculations.
        private float _circleThreshold = 1.0f; // Distance at which shark is require to return to its circle path
        private float _circleThresholdSquared; // A squared alternative to the circle Threshold to prevent frequent SQRT calculations. 

        private Vector2 _circlePosition; // The point at which the shark circles around
        private float _circleDistance; // The distance the shark circles around the crew
        private float _circlePoint = 0.0f; // The point on the circle the shark is at
        private float _circleRotationRate = 0.0083f; // The rate at which the shark circles its target
        private bool _rotateLeft; // Whether or not the shark is rotating left

        private Crew _targettedCrew; // A member of crew who the shark is circling
        private Vector2 _targetPoint; // A point which the shark has picked to move towards when it is in the searching state
        private float _targetPointDistanceThreshold = 25.0f; // A distance which is the limit before picking a new point to go to
        private float _targetPointDistanceThresholdSquared; // A squared alternative of target distance to prevent frequent SQRT calculations.
        private float _targetEdgeThreshold = 65.0f; // The threshold at which the shark will limit its random position search

        public bool angry { get { return _angry; } }
        public Vector2 targetPoint { get { return _targetPoint; } set { _targetPoint = value; } } 


        public Shark(MainGame game, Texture2D texture, Shape collisionShape, Crew targettedCrew, float circleDistance, Vector2 circlePosition, bool demoShark)
            : base(game, texture, collisionShape)
        {
            trigger = true;
            renderer.renderDepth = 0.7f;

            setSharkData(targettedCrew, circleDistance, circlePosition, demoShark, 0.8f);
        }

        public Shark(MainGame game, XmlNode node)
            : base(game, node) { }

        /// <summary>
        /// Set the sharks data, this method allows re-usability through pooling
        /// </summary>
        /// <param name="targettedCrew"></param>
        /// <param name="circleDistance"></param>
        /// <param name="circlePosition"></param>
        /// <param name="demoShark"></param>
        /// <param name="moveSpeed"></param>
        public void setSharkData(Crew targettedCrew, float circleDistance, Vector2 circlePosition, bool demoShark, float moveSpeed)
        {
            float spd = ((float)RandomUtils.random.NextDouble() * _randomSpeed) + moveSpeed;
            _moveSpeed = spd;
            maxVelocity = _moveSpeed;

            _demoShark = demoShark;
            _circlePosition = circlePosition;
            _targettedCrew = targettedCrew;
            _targetPoint = circlePosition;
            _circleDistance = circleDistance;
            _circlePoint = MathHelper.ToRadians(RandomUtils.range(0, 360));
            _rotateLeft = RandomUtils.randomBool();

            // If there is a NULL crew, we assume the shark is set to be wandering
            if (targettedCrew == null)
            {
                _searching = true;

                // Set starting position on circle.
                transform.position = circlePosition;
            }
            else
            {
                _searching = false;
                _angry = false;

                // Set starting position on circle.
                transform.position = circlePosition + getPositionAroundCircle(_circlePoint);
            }

            calculateSquaredValues();
        }

        /// <summary>
        /// Calculate squared values for distance checking, DistanceSquared offers a considerable amount of performance when Square Root calculations are used often
        /// </summary>
        private void calculateSquaredValues()
        {
            _targetPointDistanceThresholdSquared = _targetPointDistanceThreshold * _targetPointDistanceThreshold;
            _aggroDistanceSquared = _aggroDistance * _aggroDistance;
            _deAggroDistanceSquared = _deAggroDistance * _deAggroDistance;
            _circleThresholdSquared = _circleThreshold * _circleThreshold;
        }


        public override void onDraw(SpriteBatch batch)
        {
            if (game.drawDebugData && enabled)
            {
                float deg = Math.Abs(MathHelper.ToDegrees(_circlePoint) % 360);

                Vector2 a = game.gameplay.player.transform.position;
                Vector2 b = transform.position;

                Vector2 dir = Vector2.Normalize(b - a);
                Vector2 c = a + (_angry ? dir * _deAggroDistance : dir * _aggroDistance);

                if (_angry)
                {
                    game.renderManager.drawLine(a, c, Color.Yellow, 1, 0.01f);
                }
                else
                {
                    game.renderManager.drawLine(b, c, Color.Red, 1, 0.01f);
                    game.renderManager.drawLine(a, c, Color.Lime, 1, 0.01f);
                }

                // draw debug info about the sharks
                game.renderManager.drawString("Dist: " + Vector2.Distance(a, b), transform.position + new Vector2(0, -35), Color.Black);
                game.renderManager.drawString("Rot: " + deg + " , " + MathHelper.ToDegrees(_circlePoint), transform.position + new Vector2(0, -50), Color.Black);
                game.renderManager.drawString("Ang: " + _angry, transform.position + new Vector2(0, -65), Color.Black);
                game.renderManager.drawString("Search: " + _searching, transform.position + new Vector2(0, -80), Color.Black);
                game.renderManager.drawString("CPos: " + _circlePosition, transform.position + new Vector2(0, -95), Color.Black);

                string cr = "";

                if(_targettedCrew == null)
                {
                    cr = "NULL";
                }
                else
                {
                    cr = _targettedCrew.ID.ToString();
                }

                game.renderManager.drawString("Crew: " + cr, transform.position + new Vector2(0, -110), Color.Black);
            }

            base.onDraw(batch);
        }

        /// <summary>
        /// Update the AI on the shark
        /// </summary>
        /// <param name="gameTime"></param>
        void sharkAIUpdate(GameTime gameTime)
        {

            Player player = game.gameplay.player;
            Vector2 target = Vector2.Zero;
            float distance = 0.0f;

            if (_targettedCrew != null)
            {
                _circlePosition = _targettedCrew.transform.position;
            }
            else if(_targettedCrew == null)
            {
                _circlePosition = player.transform.position;
            }


            if (_angry)
            {
                distance = Vector2.DistanceSquared(transform.position, player.transform.position);

                if(distance >= _deAggroDistanceSquared)
                {
                    _angry = false;
                }
                else
                {
                    Vector2 dir = player.transform.position - transform.position;
                    addForce(dir * _moveSpeed);

                    renderer.flip = !(player.transform.position.X > transform.position.X);
                }
            }
            else if(_searching)
            {
                distance = Vector2.DistanceSquared(transform.position, player.transform.position);

                if (distance <= _aggroDistanceSquared)
                {
                    _angry = true;
                }
                else
                {
                    distance = Vector2.DistanceSquared(transform.position, _targetPoint);

                    if (distance <= _targetPointDistanceThresholdSquared)
                    {
                        _targetPoint = getRandomPositionToSearch();
                    }
                    else
                    {
                        Vector2 dir = _targetPoint - transform.position;
                        addForce(dir * _moveSpeed);

                        renderer.flip = !(_targetPoint.X > transform.position.X);
                    }
                }
            }
            else
            {
                distance = Vector2.DistanceSquared(transform.position, player.transform.position);
                if(distance <= _aggroDistanceSquared)
                {
                    _angry = true;
                }
                else
                {
                    Vector2 circleP = getPositionAroundCircle(_circlePoint);
                    float distP = Vector2.DistanceSquared(transform.position, _circlePosition + circleP); // distance between circle point and this

                    if (distP > _circleThresholdSquared) // not close enough to its circling path point
                    {
                        Vector2 dir = (_circlePosition + circleP) - transform.position;
                        addForce(dir * _moveSpeed);

                        renderer.flip = !((_circlePosition + circleP).X > transform.position.X);

                    }
                    else // on track with where it should be, circling logic..
                    {
                        if (_targettedCrew == null)
                        {
                            _searching = true;
                        }
                        else if (_targettedCrew.tag == "attachedCrew")
                        {
                            _searching = true;
                        }
                        else
                        {
                            Vector2 cPos = getPositionAroundCircle(_circlePoint);

                            rotateAroundTarget();
                            transform.position = _circlePosition + cPos;

                            float ang = MathHelper.ToDegrees(_circlePoint);

                            if (_rotateLeft)
                            {
                                renderer.flip = ang < 180.0f;
                            }
                            else
                            {
                                renderer.flip = ang > 180.0f;
                            }
                        }

                        // update and move around circle...
                    }
                }
            }

        }

        /// <summary>
        /// Rotate the sharks circle "anchor" around its circling position
        /// </summary>
        private void rotateAroundTarget()
        {
            if(_rotateLeft)
            {
                _circlePoint += _circleRotationRate;
            }
            else
            {
                _circlePoint -= _circleRotationRate;
            }

            clampCircleRotation();
        }

        /// <summary>
        /// Clamp a rotational value between 0 and 360 (degrees)
        /// </summary>
        void clampCircleRotation()
        {
            float MAX_RADIAN_ROT = MathHelper.ToRadians(360.0f);

            if (_circlePoint > MAX_RADIAN_ROT)
            {
                _circlePoint = _circlePoint - MAX_RADIAN_ROT;
            }
            else if(_circlePoint < 0.0f)
            {
                _circlePoint = _circlePoint + MAX_RADIAN_ROT;
            }
        }

        public override void update(GameTime gameTime)
        {
            sharkAIUpdate(gameTime);
            base.update(gameTime); 
            return;
        }

        /// <summary>
        /// Pick a random position for the shark to swim to
        /// </summary>
        /// <returns></returns>
        Vector2 getRandomPositionToSearch()
        {
            Vector2 bounds = game.windowBounds;
            Random r = RandomUtils.random;

            return new Vector2(r.Next((int)_targetEdgeThreshold, (int)bounds.X - (int)_targetEdgeThreshold), r.Next((int)_targetEdgeThreshold, (int)bounds.Y - (int)_targetEdgeThreshold));
        }

        /// <summary>
        /// Get a position around the circle in LOCAL SPACE
        /// </summary>
        /// <param name="rotation"></param>
        /// <returns></returns>
        Vector2 getPositionAroundCircle(float rotation)
        {
            Vector2 point = VectorUtils.rotateVector(new Vector2(_circleDistance, 0), rotation);
            return point;
        }

        /// <summary>
        /// When added to the pool, reset important values
        /// </summary>
        public void onAddedToPool()
        {
            _searching = false;
            _angry = false;
            _targettedCrew = null;
        }

        /// <summary>
        /// When released from the pool, set important values
        /// </summary>
        public void onReleasedFromPool()
        {
            enabled = true;
            renderer.enabled = true;
            _moveSpeed = _defaultMoveSpeed;
        }

        public override void saveCustomData(XmlWriter writer)
        {
            writer.WriteElementString("Angry", _angry.ToString());
            writer.WriteElementString("Searching", _searching.ToString());
            writer.WriteElementString("MoveSpeed", _moveSpeed.ToString());
            writer.WriteElementString("AggroDistance", _aggroDistance.ToString());
            writer.WriteElementString("CrewID", _targettedCrew == null ? "-1" : _targettedCrew.ID.ToString());
            writer.WriteElementString("TargetPoint",_targetPoint.ToString());
            writer.WriteElementString("RotateLeft", _rotateLeft.ToString());
            writer.WriteElementString("CircleDistance", _circleDistance.ToString());
            writer.WriteElementString("CirclePoint", _circlePoint.ToString());

            base.saveCustomData(writer);
        }

        public override void loadCustomData(XmlNode node)
        {
            _angry = bool.Parse(node.SelectSingleNode("Angry").InnerText);
            _searching = bool.Parse(node.SelectSingleNode("Searching").InnerText);
            _moveSpeed = float.Parse(node.SelectSingleNode("MoveSpeed").InnerText);
            _aggroDistance = float.Parse(node.SelectSingleNode("AggroDistance").InnerText);
            _targetPoint = IOUtils.parseVector2(node.SelectSingleNode("TargetPoint").InnerText);
            _rotateLeft = bool.Parse(node.SelectSingleNode("RotateLeft").InnerText);
            _circleDistance = float.Parse(node.SelectSingleNode("CircleDistance").InnerText);
            _circlePoint = float.Parse(node.SelectSingleNode("CirclePoint").InnerText);

            base.loadCustomData(node);
        }

        public override void postLoad(XmlNode node)
        {
            int loadedID = int.Parse(node.SelectSingleNode("CrewID").InnerText);

            if (loadedID != -1)
            {
                _targettedCrew = (Crew)game.gameObjectManager.getObject(loadedID);
                _circlePosition = _targettedCrew.transform.position;
            }

            calculateSquaredValues();

            base.postLoad(node);
        }

        /// <summary>
        /// Catch the collision event to detect player -> shark intersection
        /// </summary>
        /// <param name="other"></param>
        public override void onCollision(PhysicsObject other)
        {
            if(other.tag == "player")
            {
                Player player = (Player)other;
                player.onHitByShark(this);
            }

            base.onCollision(other);
        }

    }

}