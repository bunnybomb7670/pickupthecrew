﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Xml;

namespace PickUpTheCrew
{
    public class ParticleEmitter : GameObject
    {
        private static int _TOTAL_PARTICLE_COUNT = 0;

        private List<Particle> _particles = new List<Particle>();

        private double _time;
        private double _timeRate;
        private double _curTime;

        private bool _timedEmitter;
        private double _duration;
        private double _curDuration;

        private float _particleLife;

        private bool _emit;
        private int _particleLimit;
        private float _scale;
        private float _growth;

        private Random _random;

        /// <summary>
        /// A total (estimated) count of particles currently active across all particle emitters
        /// </summary>
        public static int TOTAL_PARTICLE_COUNT { get { return _TOTAL_PARTICLE_COUNT; } }

        /// <summary>
        /// The list of particles attached to this emitter
        /// </summary>
        public List<Particle> particles { get { return _particles; } }

        /// <summary>
        /// The rate at which the emitter spawns particles
        /// </summary>
        public double spawnRate { get { return _timeRate; } set { _timeRate = value; } }

        /// <summary>
        /// Whether or not the emitter is emitting right now
        /// </summary>
        public bool emit { get { return _emit; } set { _emit = value; } }

        /// <summary>
        /// The maximum number of particles this emitter will emit before halting
        /// </summary>
        public int particleLimit { get { return _particleLimit; } set { _particleLimit = value; } }

        /// <summary>
        /// The scale at which particles are drawn
        /// </summary>
        public float scale { get { return _scale; } set { _scale = value; } }

        /// <summary>
        /// The amount the particles grows every time they update
        /// </summary>
        public float growth { get { return _growth; } set { _growth = value; } }

        public ParticleEmitter(MainGame game, Texture2D texture, double spawnRate, int particleLimit, float particleLife, float particleScale, float particleGrowth)
            : base(game, texture, null)
        {
            _renderer = new ParticleRenderer(this, texture);

            _emit = true;
            _timeRate = spawnRate;
            _particleLimit = particleLimit;
            _particleLife = particleLife;
            _scale = particleScale;
            _growth = particleGrowth;

            _random = new Random();

            persistent = false;
        }

        /// <summary>
        /// Set the particle on a duration emission
        /// </summary>
        /// <param name="duration"></param>
        public void addDuration(float duration)
        {
            _curDuration = 0.0;
            _duration = duration;
            _timedEmitter = true;
        }

        /// <summary>
        /// Emit a particle
        /// </summary>
        /// <param name="position"></param>
        /// <param name="lifeTime"></param>
        /// <param name="rotation"></param>
        /// <param name="scale"></param>
        /// <param name="rotateLeft"></param>
        public void addParticle(Vector2 position, float lifeTime, float rotation, float scale, bool rotateLeft)
        {
            _particles.Add(new Particle(lifeTime, lifeTime, position, rotation, scale, (rotateLeft ? Direction.LEFT : Direction.RIGHT)));
            _TOTAL_PARTICLE_COUNT++;
        }

        /// <summary>
        /// Update tick which will manage the lifetime of all particles
        /// </summary>
        /// <param name="gameTime"></param>
        private void particleTick(GameTime gameTime)
        {
            for (int i = particles.Count - 1; i >= 0; i--)
            {
                if (particles[i].life <= 0)
                {
                    particles.RemoveAt(i);
                    _TOTAL_PARTICLE_COUNT--;
                }
                else
                {
                    Particle particle = particles[i];
                    particle.rotation += (0.01f * particle.rotationDirection);
                    particle.life -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                    particle.scale += _growth;
                    particles[i] = particle;
                }
            }
        }

        public override void update(GameTime gameTime)
        {
            particleTick(gameTime);

            if(_timedEmitter)
            {
                _curDuration += gameTime.ElapsedGameTime.TotalSeconds;

                if(_curDuration > _duration) // if the time has been reached, dont emit any more
                {
                    _emit = false;
                    base.update(gameTime);
                    return;
                }
            }

            _time += gameTime.ElapsedGameTime.TotalSeconds;

            if(_time > _curTime) // wait for next spawn cycle ( depending on its duration )
            {
                _curTime = _time + _timeRate;
                if (_emit && _particles.Count < particleLimit)
                {
                    addParticle(transform.position, _particleLife, _random.Next(0, 6), _scale, _random.Next(0, 11) < 5);
                }
            }

            base.update(gameTime);
        }

    }
}
