﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Xml;

namespace PickUpTheCrew
{
    public class PhysicsObject : GameObject
    {
        private Shape _shape;
        private Vector2 _velocity;
        private float _maxVelocity = 2.3f;
        private float _velocityDamping = 0.01f;
        private bool _static;
        private bool _trigger;

        /// <summary>
        /// The colliders collision shape
        /// </summary>
        public Shape shape { get { return _shape; } set { _shape = value; } }

        /// <summary>
        /// A vector representing the speed at which the object is moving in 2D space
        /// </summary>
        public Vector2 velocity { get { return _velocity; } set { _velocity = value; } }

        /// <summary>
        /// The maximum speed at which the object can go
        /// </summary>
        public float maxVelocity { get { return _maxVelocity; } set { _maxVelocity = value; } }

        /// <summary>
        /// Whether or not the object is referred to as static ( Will not take collision pushback )
        /// </summary>
        public bool colliderStatic { get { return _static; } set { _static = value; } }

        /// <summary>
        /// Whether or not the collider is a trigger ( no physical interraction, purely for detection )
        /// </summary>
        public bool trigger { get { return _trigger; } set { _trigger = value; } }

        /// <summary>
        /// The amount of damping applied to slow the object down
        /// </summary>
        public float velocityDamping { get { return _velocityDamping; } set { _velocityDamping = value; } }

        public PhysicsObject(MainGame game, Texture2D texture, Shape collisionShape)
            : base(game, texture)
        {
            _shape = collisionShape;
            game.physicsManager.registerCollider(this);
        }

        public PhysicsObject(MainGame game, XmlNode node)
            : base(game, node)
        {
            game.physicsManager.registerCollider(this);
        }

        /// <summary>
        /// Calculate and clamp the velocity between 0 and the maximum velocity
        /// </summary>
        void calculateVelocity()
        {
            Vector2 v = _velocity + (_velocity * -1) * _velocityDamping;
            _velocity = VectorUtils.truncate(v, _maxVelocity);
        }

        /// <summary>
        /// Add a force to the velocity
        /// </summary>
        /// <param name="force"></param>
        public void addForce(Vector2 force)
        {
            _velocity += force;
        }

        public override void update(GameTime gameTime)
        {
            calculateVelocity();
            transform.position += _velocity;
            base.update(gameTime);
        }

        public override void onDraw(SpriteBatch batch)
        {
            if (game.drawDebugData)
            {
                _shape.drawShape(this, game.renderManager);
            }

            base.onDraw(batch);
        }

        public override void saveCustomData(XmlWriter writer)
        {
            writer.WriteElementString("Velocity", _velocity.ToString());
            writer.WriteElementString("MaxVelocity", _maxVelocity.ToString());
            writer.WriteElementString("VelocityDamping", _velocityDamping.ToString());
            writer.WriteElementString("Static", _static.ToString());
            writer.WriteElementString("Trigger", _trigger.ToString());

            _shape.save(writer);

            base.saveCustomData(writer);
        }

        public override void loadCustomData(XmlNode node)
        {
            _velocity = IOUtils.parseVector2(node.SelectSingleNode("Velocity").InnerText);
            _maxVelocity = float.Parse(node.SelectSingleNode("MaxVelocity").InnerText);
            _velocityDamping = float.Parse(node.SelectSingleNode("VelocityDamping").InnerText);
            _static = bool.Parse(node.SelectSingleNode("Static").InnerText);
            _trigger = bool.Parse(node.SelectSingleNode("Trigger").InnerText);

            _shape = new Shape(node.SelectSingleNode("Shape/Points"));

            base.loadCustomData(node);
        }

        /// <summary>
        /// A virtual method used by objects which inherit to catch collision "events"
        /// </summary>
        /// <param name="other"></param>
        public virtual void onCollision(PhysicsObject other) { }
    }
}