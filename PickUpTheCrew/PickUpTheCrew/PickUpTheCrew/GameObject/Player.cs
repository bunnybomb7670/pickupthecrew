﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using System.Xml;

namespace PickUpTheCrew
{
    public class Player : PhysicsObject, IAttachable
    {
        // Movement values
        private float _baseMoveSpeed = 10.0f;
        private float _moveSpeed = 10.0f;
        private float _rotationSpeed = 0.03f;
        private Vector2 _emitterOffset = new Vector2(0, 32);
        private float _engineRev = 0.0f;

        // GAME EXTRA : HEAVY CREW
        private float _crewWeightModifier = 1.0f;

        // Splash Emitter values
        private float _splashEmitterRate = 0.05f;
        private int _splashEmitterParticleLimit = 50;
        private float _splashEmitterParticleLifetime = 2.3f;
        private float _splashEmitterParticleScale = 4;
        private float _splashEmitterParticleGrowth = 0.05f;

        // Poof Emitter values
        private float _poofEmitterRate = 0.025f;
        private int _poofemitterParticleLimit = 100;
        private float _poofemitterParticleLifetime = 3.3f;
        private float _poofemitterParticleScale = 12;
        private float _poofemitterParticleGrowth = 0.05f;

        // Duration of poof emitter
        private float _poofDuration = 0.25f;

        // Crew list
        private List<Crew> _crew = new List<Crew>();

        // Emitters
        private ParticleEmitter _splashEmitter;
        private ParticleEmitter _poofEmitter;

        /// <summary>
        /// Whether or not all crew have been collected
        /// </summary>
        public bool crewCollected { get { return _crew.Count == 6; } }

        public Player(MainGame game, Texture2D texture, Shape collisionShape)
            : base(game, texture, collisionShape)
        {
            tag = "player";
            renderer.renderDepth = 0.5f;
            createEmitters();
            game.gameplay.setPlayer(this);
        }

        public Player(MainGame game, XmlNode node)
            : base(game, node)
        {
            game.gameplay.setPlayer(this);
        }

        /// <summary>
        /// Create the partice emitters for the object
        /// </summary>
        private void createEmitters()
        {
            _poofEmitter = new ParticleEmitter(game, TextureManager.getTexture("poof"), _poofEmitterRate, _poofemitterParticleLimit, _poofemitterParticleLifetime, _poofemitterParticleScale, _poofemitterParticleGrowth);
            _poofEmitter.transform.position = new Vector2(-500, -500);
            _poofEmitter.renderer.renderDepth = 0.01f;
            _poofEmitter.emit = false;

            _splashEmitter = new ParticleEmitter(game, TextureManager.getTexture("waterRipple"), _splashEmitterRate, _splashEmitterParticleLimit, _splashEmitterParticleLifetime, _splashEmitterParticleScale, _splashEmitterParticleGrowth);
            _splashEmitter.transform.position = new Vector2(-500, -500);
            _splashEmitter.renderer.renderDepth = 0.9f;
        }

        /// <summary>
        /// Get the point at which the attachment is for the crew chain ( the chain gets progressively longer and should attach at the tail )
        /// </summary>
        /// <returns></returns>
        public Vector2 getCrewAttachmentPoint()
        {
            if (_crew.Count == 0)
            {
                return this.transform.position;
            }
            else
            {
                Crew cr = _crew[_crew.Count - 1];
                return cr.transform.position;
            }
        }

        /// <summary>
        /// Reset the speed to the default speed
        /// </summary>
        public void resetSpeed()
        {
            _moveSpeed = _baseMoveSpeed;
        }

        /// <summary>
        /// Attach a crew to the chain
        /// </summary>
        /// <param name="crewObject"></param>
        public void attachCrew(PhysicsObject crewObject)
        {
            Crew c = (Crew)crewObject;
            _moveSpeed -= _crewWeightModifier;
            game.gameplay.onCrewPickedUp(c.crewType);
            c.attachCrew(_crew.Count == 0 ? (PhysicsObject)this : _crew[_crew.Count - 1]);
            _crew.Add(c);
        }

        /// <summary>
        /// Clear all the crew from the players chain
        /// </summary>
        public void clearCrew()
        {
            _crew.Clear();
        }

        public override void saveCustomData(XmlWriter writer)
        {
            writer.WriteElementString("MoveSpeed", _moveSpeed.ToString());
            writer.WriteElementString("RotationSpeed", _rotationSpeed.ToString());
            writer.WriteElementString("EmitterOffset", _emitterOffset.ToString());
            writer.WriteElementString("EngineRev", _engineRev.ToString());

            base.saveCustomData(writer);
        }

        public override void loadCustomData(XmlNode node)
        {
            _moveSpeed = float.Parse(node.SelectSingleNode("MoveSpeed").InnerText);
            _rotationSpeed = float.Parse(node.SelectSingleNode("RotationSpeed").InnerText);
            _emitterOffset = IOUtils.parseVector2(node.SelectSingleNode("EmitterOffset").InnerText);
            _engineRev = float.Parse(node.SelectSingleNode("EngineRev").InnerText);

            base.loadCustomData(node);
        }

        public override void postLoad(XmlNode node)
        {
            createEmitters();
            base.postLoad(node);
        }

        public override void onUnload()
        {
            game.gameObjectManager.unregisterGameObject(_splashEmitter);
            game.gameObjectManager.unregisterGameObject(_poofEmitter);
            base.onUnload();
        }

        public override void update(GameTime gameTime)
        {
            // Sync emitter to player with an offset
            _splashEmitter.transform.position = VectorUtils.rotateVectorAroundPointWithOffset(transform.position, _emitterOffset, transform.rotation);
            _poofEmitter.transform.position = transform.position;

            bool rev = false;

            // Rotation
            if (InputUtils.isKeyDown(Keys.Left))
            {
                transform.rotate(-_rotationSpeed);
            }

            if (InputUtils.isKeyDown(Keys.Right))
            {
                transform.rotate(_rotationSpeed);
            }

            // Movement relative to forward direction
            if (InputUtils.isKeyDown(Keys.Up))
            {
                velocity += (transform.forward * (_moveSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds));
                rev = true;
            }

            if (InputUtils.isKeyDown(Keys.Down))
            {
                velocity -= (transform.forward * ((_moveSpeed / 2) * (float)gameTime.ElapsedGameTime.TotalSeconds));
                rev = true;
            }

            // Calculate particle emission
            if(rev)
            {
                _engineRev = MathHelper.Clamp(_engineRev + (45.0f * (float)gameTime.ElapsedGameTime.TotalSeconds), 0, 100.0f);
            }
            else
            {
                _engineRev = MathHelper.Clamp(_engineRev - (45.0f * (float)gameTime.ElapsedGameTime.TotalSeconds), 0, 100.0f);
            }

            _splashEmitter.emit = !(_engineRev <= 0.0);

            base.update(gameTime);
        }

        /// <summary>
        /// When the player is hit by a shark
        /// </summary>
        /// <param name="shark"></param>
        public void onHitByShark(Shark shark)
        {
            game.gameplay.gameEnd();
            _moveSpeed = _baseMoveSpeed;
            _splashEmitter.transform.position = new Vector2(-500, -500);
            explode();
        }

        /// <summary>
        /// Explode the player into a puff of smoke ( schedul a particle emission burst )
        /// </summary>
        public void explode()
        {
            enabled = false;
            _poofEmitter.addDuration(_poofDuration);
            _poofEmitter.emit = true;
        }

        /// <summary>
        /// Get the default anchor position
        /// </summary>
        /// <returns>Vector2 containing the default anchor position</returns>
        public Vector2 getAnchor()
        {
            return VectorUtils.rotateVectorAroundPointWithOffset(transform.position, new Vector2(0, 65.0f), transform.rotation);
        }

        /// <summary>
        /// Get the anchor which is attached to the top of the object
        /// </summary>
        /// <returns>Vector2 containing the top anchor</returns>
        public Vector2 getTopAnchor()
        {
            return VectorUtils.rotateVectorAroundPointWithOffset(transform.position, new Vector2(0, 25.0f), transform.rotation);
        }

        /// <summary>
        /// Get the anchor which is attached to the bottom of the object
        /// </summary>
        /// <returns>Vector2 containing the bottom anchor</returns>
        public Vector2 getBottomAnchor()
        {
            return VectorUtils.rotateVectorAroundPointWithOffset(transform.position, new Vector2(0, 25.0f), transform.rotation);
        }
    }
}