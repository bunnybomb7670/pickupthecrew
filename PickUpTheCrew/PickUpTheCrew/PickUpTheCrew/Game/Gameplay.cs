﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using System.Xml;

namespace PickUpTheCrew
{
    public class Gameplay
    {
        private MainGame _game;
        private GameSpawner _gameSpawner;
        private GameStats _gameStats;
        private Highscores _highscores;

        private string HIGHSCOREPATH = "highscores.xml";
        private int MAXHIGHSCORES = 5;
        private Keys STARTKEY = Keys.Space;

        // Transition mode values
        private double _time = 0.0f;
        private double _transitionTimerate = 3.5f;

        // Title waving
        private float _sinValue = 0.0f;

        // Crew visualization
        private double _nameTime = 0.0f;
        private double _nameTimeRate = 10.0f;

        // Demo values
        private bool _newHighscore = false;
        private int _newHighscoreIndex = 0;

        // Demo objects
        private List<Shark> demoSharks = new List<Shark>();

        // Game objects
        private Player _player;
        public List<Shark> _sharks = new List<Shark>();
        public List<Crew> _crew = new List<Crew>();

        private int _demoSharkCount = 8;

        /// <summary>
        /// MainGame reference
        /// </summary>
        public MainGame game { get { return _game; } }

        /// <summary>
        /// GameSpawner reference
        /// </summary>
        public GameSpawner gameSpawner { get { return _gameSpawner; } }

        /// <summary>
        /// Player reference
        /// </summary>
        public Player player { get { return _player; } }

        /// <summary>
        /// The gameStats reference
        /// </summary>
        public GameStats gameStats { get { return _gameStats; } }

        /// <summary>
        /// The highscores reference
        /// </summary>
        public Highscores highscores { get { return _highscores; } }

        /// <summary>
        /// The total amount of attract mode sharks present
        /// </summary>
        public int demoSharkCount { get { return demoSharks.Count; } }

        /// <summary>
        /// The percentage of alpha (transparency) left for the game-over message
        /// </summary>
        public float transitionTimePercent { get { return 100.0f - ((float)(_time / (_transitionTimerate-0.4f)) * 100.0f); } }

        /// <summary>
        /// The percent of alpha (transparency) left for the name-fading for crew announcements
        /// </summary>
        public float nameFadePercent { get { return (100.0f - ((float)(_nameTime / (_nameTimeRate) * 100.0f))); } }

        /// <summary>
        /// Whether or not a new highscore has been gained
        /// </summary>
        public bool newHighscore { get { return _newHighscore; } }

        /// <summary>
        /// The index of the new highscore (if one is present)
        /// </summary>
        public int newHighscoreIndex { get { return _newHighscoreIndex; } }

        /// <summary>
        /// The sine-wave value used for having waving titles
        /// </summary>
        public float titleSinWaveValue { get { return _sinValue; } }

        public Gameplay(MainGame game)
        {
            _game = game;
            _gameSpawner = new GameSpawner(game);
            _gameStats = new GameStats(game);

            createHighscores();
        }

        /// <summary>
        /// Create the highscore instance and either load it or create an empty score file
        /// </summary>
        void createHighscores()
        {
            _highscores = new Highscores(MAXHIGHSCORES);

            if (IOUtils.checkForFile(HIGHSCOREPATH))
            {
                XmlDocument doc = new XmlDocument();

                doc.Load(HIGHSCOREPATH);
                XmlNode scoreNode = doc.SelectSingleNode("Scores");
                _highscores.load(scoreNode);
            }
            else // no highscores file exists, make an empty one...
            {
                saveScores();             
            }
        }

        /// <summary>
        /// Save the highscores to the XML file
        /// </summary>
        public void saveScores()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";
            settings.NewLineOnAttributes = true;

            XmlWriter writer = XmlWriter.Create(HIGHSCOREPATH, settings);

            _highscores.save(writer);

            writer.Close();
        }

        /// <summary>
        /// When a crew has been picked up (update visible text)
        /// </summary>
        /// <param name="crewType"></param>
        public void onCrewPickedUp(CrewType crewType)
        {
            _gameStats.pickupCrew();

            _nameTime = 0.0f;
            _game.renderManager.lastCollectedCrewType = crewType;
            _game.renderManager.lastCollectedCrewTypeVisible = true;
        }

        /// <summary>
        /// Called every frame..
        /// </summary>
        /// <param name="gameTime"></param>
        public void update(GameTime gameTime)
        {
            GameState state = _game.stateController.gameState;

            switch(state)
            {
                case GameState.DEMO:

                    _sinValue += (float)gameTime.ElapsedGameTime.TotalSeconds;

                    if (InputUtils.isKeyPressed(STARTKEY))
                    {
                        _game.stateController.stateTransition(GameState.PLAY,false);
                    }

                    break;

                case GameState.PLAY:

                    _nameTime += gameTime.ElapsedGameTime.TotalSeconds;

                    if(_nameTime > _nameTimeRate)
                    {
                        _game.renderManager.lastCollectedCrewTypeVisible = false;
                    }

                    break;

                case GameState.TRANSITION:

                    _time += gameTime.ElapsedGameTime.TotalSeconds;
                    
                    if(_time > _transitionTimerate)
                    {
                        _game.stateController.stateTransition(GameState.DEMO, false);
                    }

                    break;
            }
        }

        /// <summary>
        /// Intialize all game assets ready for play
        /// </summary>
        public void initializeGameAssets()
        {
            // Create the background
            Vector2 b = _game.windowBounds;
            Vector2 m = new Vector2(b.X / 2, b.Y / 2);

            int animRepeatX = (int)(b.X / 32);
            int animRepeatY = (int)(b.Y / 32);

            Animator anim = new Animator(_game, TextureManager.getTexture("waterSheet2"), 8, 8, 31, 0.0625f, new Vector2(32, 32), animRepeatX, animRepeatY);
            anim.transform.position = new Vector2(0, 0);
            anim.transform.scale = 2.0f;
            anim.destroyOnLoad = false;
            anim.tag = "anim";

            initializeDemoObjects();
            initializeGameObjects();
        }

        /// <summary>
        /// Create and initialize all game assets
        /// </summary>
        private void initializeGameObjects()
        {
            Shape shipShape = new Shape(new Vector2[] { new Vector2(0, -0.3f), new Vector2(-0.1f, -0.2f), new Vector2(-0.15f, 0.1f), new Vector2(-0.1f, 0.3f), new Vector2(0.1f, 0.3f), new Vector2(0.15f, 0.1f), new Vector2(0.1f, -0.2f) }, 100);

            Player p = new Player(_game, TextureManager.getTexture("ship"), shipShape);

            resetPlayer();
        }

        /// <summary>
        /// When the game has loaded, reset data...
        /// </summary>
        public void onGameLoaded()
        {
            _crew.Clear();
            _sharks.Clear();
            _player = null;
        }

        /// <summary>
        /// Create and store all demo assets
        /// </summary>
        private void initializeDemoObjects()
        {
            for(int i = 0; i < _demoSharkCount; i++)
            {
                Shark s = game.gameObjectManager.getPooledShark();
                s.setSharkData(null, 0.0f, Vector2.Zero, true, _gameStats.sharkMoveSpeed());
                s.destroyOnLoad = false;
                s.persistent = false;
                demoSharks.Add(s);
            }
        }

        /// <summary>
        /// Set the Crew and Shark lists
        /// </summary>
        /// <param name="sharks"></param>
        /// <param name="crew"></param>
        public void setObjectList(List<Shark> sharks, List<Crew> crew)
        {
            _sharks = sharks;
            _crew = crew;
        }

        /// <summary>
        /// Set the player reference
        /// </summary>
        /// <param name="player"></param>
        public void setPlayer(Player player)
        {
            _player = player;
        }

        /// <summary>
        /// Set the crew list reference
        /// </summary>
        /// <param name="crew"></param>
        public void setCrew(List<Crew> crew)
        {
            _crew = crew;
        }

        /// <summary>
        /// Set the shark list reference
        /// </summary>
        /// <param name="sharks"></param>
        public void setSharks(List<Shark> sharks)
        {
            _sharks = sharks;
        }

        /// <summary>
        /// Reset the player and any of its data
        /// </summary>
        void resetPlayer()
        {
            Vector2 b = _game.windowBounds;
            Vector2 m = new Vector2(b.X / 2, b.Y / 2);

            _player.transform.position = m;
            _player.velocity = Vector2.Zero;
            _player.transform.rotation = 0.0f;
            _player.resetSpeed();
            _player.enabled = true;

            player.clearCrew();
        }

        /// <summary>
        /// Toggle the attract mode sharks on and off
        /// </summary>
        /// <param name="state"></param>
        void toggleDemoSharks(bool state)
        {
            Random rand = RandomUtils.random;
            Vector2 bounds = game.windowBounds;
            int mod = 150;

            if(state)
            {
                for(int i = 0; i < demoSharks.Count; i++)
                {
                    Shark s = demoSharks[i];
                    s.enabled = true;
                    s.transform.position = new Vector2(rand.Next(mod, (int)bounds.X - mod), rand.Next(mod, (int)bounds.Y - mod));
                    s.targetPoint = s.transform.position;
                }
            }
            else
            {
                for (int i = 0; i < demoSharks.Count; i++)
                {
                    Shark s = demoSharks[i];
                    s.enabled = false;
                    s.transform.position = new Vector2(-500, -500);
                }
            }
        }

        /// <summary>
        /// Store all sharks and crew in the pool after a game has finished
        /// </summary>
        void poolSharksAndCrew()
        {
            foreach (Shark s in _sharks)
            {
                _game.gameObjectManager.storeSharkInPool(s);
            }

            foreach (Crew c in _crew)
            {
                _game.gameObjectManager.storeCrewInPool(c);
            }

            _sharks.Clear();
            _crew.Clear();
        }

        /// <summary>
        /// When all crew have been collected and the player has hit the edge, increment the level
        /// </summary>
        public void nextLevel()
        {
            _nameTime = 0.0f;
            _gameStats.nextLevel();
            poolSharksAndCrew();
            resetPlayer();
            _gameSpawner.spawnCrewAndSharks(_gameStats.sharks, 3, 2);
        }

        /// <summary>
        /// Game starting, re-position all "game assets"
        /// </summary>
        public void gameStart()
        {
            _newHighscore = false;
            _nameTime = 0.0f;
            _gameStats.gameStart();
            poolSharksAndCrew();
            resetPlayer();
            RandomUtils.resetLevelRandom(_game.randomSeed);
            _gameSpawner.spawnCrewAndSharks(_gameStats.sharks, 3, 2);
        }

        /// <summary>
        /// Game ending, store all "game assets"
        /// </summary>
        public void gameEnd()
        {
            game.stateController.stateTransition(GameState.TRANSITION, false);
        }

        /// <summary>
        /// Demo starting, re-position all "demo assets"
        /// </summary>
        public void demoStart()
        {
            player.enabled = false;
            player.transform.position = new Vector2(-500, -500);

            toggleDemoSharks(true);
        }

        /// <summary>
        /// Demo ending, store all "demo assets"
        /// </summary>
        public void demoEnd()
        {
            toggleDemoSharks(false);
        }

        /// <summary>
        /// Start of the game-over stage, catch the highscore
        /// </summary>
        public void transitionStart()
        {
            int newIndex = 0;
            _newHighscore = _highscores.submitScore(_gameStats.score, out newIndex);
            _newHighscoreIndex = newIndex;
        }

        /// <summary>
        /// End of game-over stage, back to demo-mode
        /// </summary>
        public void transitionEnd()
        {
            poolSharksAndCrew();
            player.clearCrew();
            _time = 0.0f;
        }

    }
}
