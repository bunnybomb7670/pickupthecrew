﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PickUpTheCrew
{
    public class GameSpawner
    {
        private MainGame _game;

        private float _edgeBoundsModifier = 120; // The range inwards from the edge of the screen in which spawning is limited.
        private float _insideBoundsModifier = 150; // The range inwards from WITHIN bounds to prevent overlapping sector spawning.
        private Vector2 _playerZone = new Vector2(600, 600); // The restriction zone placed around the player to prevent spawning sharks too close.
        private float _sharkCircleDiameter = 80; // The distance at which sharks circle around their crew members.

        public GameSpawner(MainGame game)
        {
            _game = game;
        }

        /// <summary>
        /// Dynamically generate crew and sharks in an even distribution across the space of 
        /// the screen by using zoning techniques to split up the screen and allocate certain area's.
        /// </summary>
        /// <param name="sharkCount"></param>
        /// <param name="splitX"></param>
        /// <param name="splitY"></param>
        public void spawnCrewAndSharks(int sharkCount, int splitX, int splitY)
        {
            List<Shark> sharksL = new List<Shark>();
            List<Crew> crewL = new List<Crew>();

            Vector2 bounds = _game.windowBounds;

            float sizeX = bounds.X / splitX;
            float sizeY = bounds.Y / splitY;

            int count = 0;

            // Create crew distribution points
            for (int i = 0; i < splitX; i++) // iterate X
            {
                for (int j = 0; j < splitY; j++) // iterate Y
                {
                    // Calculate bounds for check zone and take into account edge sectors to limit
                    float offsetL = (i > 0 && i <= splitX - 1) ? _insideBoundsModifier : 0;
                    float offsetR = (i >= 0 && i < splitX - 1) ? -_insideBoundsModifier : 0;
                    float offsetT = (j > 0 && j <= splitY - 1) ? -_insideBoundsModifier : 0;
                    float offsetB = (j >= 0 && j < splitY - 1) ? _insideBoundsModifier : 0;

                    float left = MathHelper.Clamp(((sizeX * i) + offsetL), _edgeBoundsModifier, bounds.X - _edgeBoundsModifier);
                    float right = MathHelper.Clamp((((sizeX * i) + sizeX) + offsetR), 0, bounds.X - _edgeBoundsModifier);
                    float bottom = MathHelper.Clamp(((sizeY * j) + offsetB), _edgeBoundsModifier, bounds.Y - _edgeBoundsModifier);
                    float top = MathHelper.Clamp((((sizeY * j) + sizeY) + offsetT), 0, bounds.Y - _edgeBoundsModifier);

                    // Position for the shark / crew to be spawned around
                    Vector2 pos = getPointWithinBoundsExcludingPlayerZone((int)left, (int)right, (int)bottom, (int)top, _playerZone);

                    // Create the crew to be circled
                    Crew crew = _game.gameObjectManager.getPooledCrew();
                    crew.transform.position = pos;
                    crew.enabled = true;
                    crew.renderer.enabled = true;
                    crew.setCrewData(getCrewType(count));

                    // Create the shark to circle
                    Shark shark = _game.gameObjectManager.getPooledShark();
                    shark.setSharkData(crew, _sharkCircleDiameter, pos, false, _game.gameplay.gameStats.sharkMoveSpeed());

                    sharksL.Add(shark);
                    crewL.Add(crew);

                    count++;
                }
            }

            int remainder = sharkCount - count;

            for (int i = 0; i < remainder; i++) // Create an extra sharks which wander around
            {
                float left = _edgeBoundsModifier;
                float right = bounds.X - _edgeBoundsModifier;
                float bottom = _edgeBoundsModifier;
                float top = bounds.Y - _edgeBoundsModifier;

                Vector2 pos = getPointWithinBoundsExcludingPlayerZone((int)left, (int)right, (int)bottom, (int)top, _playerZone);

                Shark shark = _game.gameObjectManager.getPooledShark();
                shark.setSharkData(null, _sharkCircleDiameter, pos, false, _game.gameplay.gameStats.sharkMoveSpeed());

                sharksL.Add(shark);
            }

            _game.gameplay.setObjectList(sharksL, crewL);

        }

        /// <summary>
        /// Get a crew type from an index (used for easier spawning)
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        CrewType getCrewType(int index)
        {
            switch (index)
            {
                case 0: return CrewType.Lieutenant_Sky;
                case 1: return CrewType.Midshipman_Verde;
                case 2: return CrewType.Cabinboy_Crimson;
                case 3: return CrewType.Stoker_Orange;
                case 4: return CrewType.Able_Seaman_Shocking_Pink;
                case 5: return CrewType.Commander_Custard;
                default: throw new Exception("Failed to get crew type");
            }
        }

        /// <summary>
        /// Get a point within the bounds provided but excluding the bounds around the player.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <param name="bottom"></param>
        /// <param name="top"></param>
        /// <param name="playerZoneSize"></param>
        /// <returns></returns>
        Vector2 getPointWithinBoundsExcludingPlayerZone(int left, int right, int bottom, int top, Vector2 playerZoneSize)
        {
            Vector2 bounds = _game.windowBounds;
            Vector2 m = new Vector2(bounds.X / 2, bounds.Y / 2);

            Random rand = RandomUtils.levelRandom;

            Vector2 pos = new Vector2(rand.Next(left, right), rand.Next(bottom, top));

            int l = (int)(m.X - (playerZoneSize.X / 2));
            int r = (int)(m.X + (playerZoneSize.X / 2));
            int b = (int)(m.Y - (playerZoneSize.Y / 2));
            int t = (int)(m.Y + (playerZoneSize.Y / 2));

            Vector2 c = clampPointOutsideRectangle(pos, l, r, b, t);

            return c;
        }

        /// <summary>
        /// Clamp a position and push it OUTSIDE a rectangle area
        /// </summary>
        /// <param name="point"></param>
        /// <param name="clampRectangle"></param>
        /// <returns></returns>
        Vector2 clampPointOutsideRectangle(Vector2 point, int left, int right, int bottom, int top)
        {
            Vector2 mid = new Vector2(left + ((right - left) / 2), bottom + ((top - bottom) / 2)); // find middle point

            float x = point.X;
            float y = point.Y;

            if (point.X >= left && point.X <= right && point.Y >= bottom && point.Y <= top) // within bounds?
            {
                float distXRaw = mid.X - point.X;
                float distYRaw = mid.Y - point.Y;
                float distX = Math.Abs(distXRaw);
                float distY = Math.Abs(distYRaw);

                if (distX > distY)
                {
                    if (distXRaw >= 0) // right
                    {
                        x = right;
                    }
                    else // left
                    {
                        x = left;
                    }
                }
                else
                {
                    if (distYRaw >= 0) // top
                    {
                        y = top;
                    }
                    else // bottom
                    {
                        y = bottom;
                    }
                }
            }

            return new Vector2(x, y);
        }
    }
}