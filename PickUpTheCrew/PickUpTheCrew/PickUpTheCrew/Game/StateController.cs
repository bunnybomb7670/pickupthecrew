﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PickUpTheCrew
{
    public class StateController
    {
        private GameState _gameState;
        private MainGame _game;

        /// <summary>
        /// The current state which the FSM is on
        /// </summary>
        public GameState gameState { get { return _gameState; } }

        /// <summary>
        /// Reference to the main game instance
        /// </summary>
        public MainGame game { get { return _game; } }

        public StateController(MainGame game)
        {
            _game = game;
            _gameState = GameState.INITIALIZE;
        }

        /// <summary>
        /// Transition between states and manage the transition aswell as prevent unwanted state transitions.
        /// </summary>
        /// <param name="state"></param>
        public void stateTransition(GameState state, bool loadedGameTransition)
        {
            GameState prev = _gameState;
            _gameState = state;

            if (prev == state)
            {
                return;
            }

            switch (prev)
            {
                // Transition from INITIALIZE
                // Destinations:
                // Demo
                case GameState.INITIALIZE:

                    if (state == GameState.DEMO)
                    {
                        _game.gameplay.demoStart();
                    }
                    else
                    {
                        throw new Exception("Invalid State Transition INITIALIZE -> " + state + "not supported.");
                    }

                    break;

                // Transition from DEMO
                // Destinations:
                // PLAY
                case GameState.DEMO:
                    if (state == GameState.PLAY)
                    {
                        _game.gameplay.demoEnd();
                        if (!loadedGameTransition)
                        {
                            _game.gameplay.gameStart();
                        }
                    }
                    else
                    {
                        throw new Exception("Invalid State Transition DEMO -> " + state + " not supported.");
                    }

                    break;

                // Transition from PLAY
                // Destinations:
                // TRANSITION
                case GameState.PLAY:

                    if (state == GameState.TRANSITION)
                    {
                        _game.gameplay.gameEnd();
                        _game.gameplay.transitionStart();
                    }
                    else
                    {
                        throw new Exception("Invalid State Transition PLAY -> " + state + " not supported.");
                    }

                    break;

                // Transition from TRANSITION
                // Destinations:
                // DEMO
                case GameState.TRANSITION:

                    if (state == GameState.DEMO)
                    {
                        _game.gameplay.transitionEnd();
                        _game.gameplay.demoStart();
                    }
                    else
                    {
                        throw new Exception("Invalid State Transition TRANSITION -> " + state + " not supported.");
                    }

                    break;
            }
        }
    }
}