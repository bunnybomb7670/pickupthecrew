﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;

using Microsoft.Xna.Framework;

using System.IO;

namespace PickUpTheCrew
{

    public class GameStats
    {
        private MainGame _game;

        private int _level = 1; // level we are currently on
        private int _score = 0; // score we are currently on

        private int _sharkModifier = 2; // how many levels before an extra shark is spawned
        private int _scoreModifier = 4;
        private int _extraSharks = 0; // how many extra sharks to spawn

        private float _baseSpeed = 0.55f;
        private float _speedModifier = 0.025f;

        // The amount of sharks to spawn
        public int sharks { get { return 6 + _extraSharks; } }

        /// <summary>
        /// The score the player currently has
        /// </summary>
        public int score { get { return _score; } }

        /// <summary>
        /// The level the player is currently on
        /// </summary>
        public int level { get { return _level; } }

        public GameStats(MainGame game)
        {
            _game = game;
        }

        /// <summary>
        /// When a crew member has been picked up, increment score
        /// </summary>
        public void pickupCrew()
        {
            _score += (20 * (_scoreModifier * _level));
        }

        /// <summary>
        /// Get the shark move speed based on modifiers
        /// </summary>
        /// <returns></returns>
        public float sharkMoveSpeed()
        {
            return _baseSpeed + (_speedModifier * _level);
        }

        /// <summary>
        /// When the game starts, reset values
        /// </summary>
        public void gameStart()
        {
            _extraSharks = 0;
            _level = 1;
            _score = 0;
        }

        /// <summary>
        /// Run the next level logic and increase modifiers
        /// </summary>
        public void nextLevel()
        {
            _level++;

            if (_level % _sharkModifier == 0)
            {
                _extraSharks++;
            }
        }

        /// <summary>
        /// Save the game stats
        /// </summary>
        /// <param name="writer"></param>
        public void save(XmlWriter writer)
        {
            writer.WriteStartElement("Stats");
            writer.WriteElementString("Level", _level.ToString());
            writer.WriteElementString("Score", _score.ToString());
            writer.WriteElementString("ExtraSharks", _extraSharks.ToString());
            writer.WriteEndElement();
        }

        /// <summary>
        /// Load the game stats
        /// </summary>
        /// <param name="node"></param>
        public void load(XmlNode node)
        {
            _level = int.Parse(node.SelectSingleNode("Level").InnerText);
            _score = int.Parse(node.SelectSingleNode("Score").InnerText);
            _extraSharks = int.Parse(node.SelectSingleNode("ExtraSharks").InnerText);
        }
    }
}