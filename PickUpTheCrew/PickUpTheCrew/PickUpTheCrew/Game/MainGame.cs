using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using System.Xml;

namespace PickUpTheCrew
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class MainGame : Game
    {
        private string _TEXTUREPATH = "Textures";
        private string _FONTPATH = "Fonts";
        private string _SAVEPATH = "GameData.xml";
        private int _WIDTH;
        private int _HEIGHT;
        private int _RANDOMSEED = 76701337;
        private bool _FULLSCREEN = true;
        private Keys _DEBUGTOGGLEKEY = Keys.Q;
        private Keys _SAVEKEY = Keys.S;
        private Keys _LOADKEY = Keys.L;

        private GraphicsDeviceManager graphics;
        private GameObjectManager _gameObjectManager;
        private RenderManager _renderManager;
        private PhysicsManager _physicsManager;
        private Gameplay _gameplay;
        private StateController _stateController;
        private ScreenBounds _screenBounds;
        private bool _drawDebugData = false;
        private FPSCalculator _fpsCalculator;
        private Vector2 _windowBounds;

        /// <summary>
        /// Reference to the gameobject management class
        /// </summary>
        public GameObjectManager gameObjectManager { get { return _gameObjectManager; } }

        /// <summary>
        /// Public reference to the private seed stored
        /// </summary>
        public int randomSeed { get { return _RANDOMSEED; } }

        /// <summary>
        /// Reference to the render manager
        /// </summary>
        public RenderManager renderManager { get { return _renderManager; } }

        /// <summary>
        /// Reference to the physics manager
        /// </summary>
        public PhysicsManager physicsManager { get { return _physicsManager; } }

        /// <summary>
        /// Reference to the gameplay class which manages all game-play elements
        /// </summary>
        public Gameplay gameplay { get { return _gameplay; } }

        /// <summary>
        /// The FSM (Finite State Machine) object which controls the games state
        /// </summary>
        public StateController stateController { get { return _stateController; } }

        /// <summary>
        /// The bounds of the window (size)
        /// </summary>
        public Vector2 windowBounds { get { return _windowBounds; } }

        /// <summary>
        /// Reference to the FPS calculator
        /// </summary>
        public FPSCalculator fpsCalculator { get { return _fpsCalculator; } }

        /// <summary>
        /// Whether or not to draw debug info (collision boxes etc)
        /// </summary>
        public bool drawDebugData { get { return _drawDebugData; } }

        public MainGame()
        {
            _WIDTH = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            _HEIGHT = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;

            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = _WIDTH,
                PreferredBackBufferHeight = _HEIGHT,
                IsFullScreen = _FULLSCREEN
            };

            _windowBounds = new Vector2(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);

            this.IsMouseVisible = false;

            Content.RootDirectory = "Content";

            int startID = 0;
            _gameObjectManager = new GameObjectManager(this, startID);
            _physicsManager = new PhysicsManager(this);
        }

        /// <summary>
        /// When the game is about to exit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected override void OnExiting(object sender, EventArgs args)
        {
            gameplay.saveScores();

            base.OnExiting(sender, args);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            _fpsCalculator = new FPSCalculator();
            _stateController = new StateController(this);
            RandomUtils.InitializeRandomWithSeed(_RANDOMSEED);
            RandomUtils.resetLevelRandom(_RANDOMSEED);

            base.Initialize();
        }

        /// <summary>
        /// Called after Initialize and LoadContent. Helps to initialize things which rely 
        /// on data loaded from the LoadContent method.
        /// </summary>
        private void PostInitialize()
        {
            _renderManager = new RenderManager(this, GraphicsDevice);
            _screenBounds = new ScreenBounds(this);
            _gameplay = new Gameplay(this);
            _gameplay.initializeGameAssets();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Load all resources
            TextureManager.loadTextures(Content, _TEXTUREPATH);
            FontManager.loadFonts(Content, _FONTPATH);
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Calculate FPS
            _fpsCalculator.update(gameTime);

            if (InputUtils.isKeyDown(Keys.Escape))
            {
                this.Exit();
            }
            else if (InputUtils.isKeyPressed(_DEBUGTOGGLEKEY))
            {
                _drawDebugData = !drawDebugData;
            }

            // Manage game state machine
            stateUpdate();

            // Update all GameObjects
            _gameObjectManager.update(gameTime);

            // Update game logic
            _gameplay.update(gameTime);

            // Update physics
            _physicsManager.updatePhysics();

            // Update GamePlay Logic
            _gameplay.update(gameTime);

            // Manage DEBUG logic here ::

            //Save game management
            if (InputUtils.isKeyPressed(_SAVEKEY) && _stateController.gameState == GameState.PLAY)
            {
                saveGame();
            }

            //Load game management
            else if (InputUtils.isKeyPressed(_LOADKEY) && (_stateController.gameState == GameState.PLAY || _stateController.gameState == GameState.DEMO))
            {
                if (IOUtils.checkForFile(_SAVEPATH))
                {
                    loadGame();
                }
            }

            // Update key pressed states
            InputUtils.updateState();

            // End of DEBUG logic

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            _fpsCalculator.frameUpdate();

            _renderManager.debugDrawing = _drawDebugData;
            _renderManager.debugColliderDrawing = _drawDebugData;

            // Draw every renderer
            _renderManager.draw(gameTime);

            // Draw all UI
            _renderManager.drawUI(gameTime, _stateController.gameState);

            base.Draw(gameTime);
        }

        /// <summary>
        /// Handle state machine logic per frame
        /// </summary>
        private void stateUpdate()
        {
            GameState s = _stateController.gameState;

            if (s == GameState.INITIALIZE) // Catch initialization state
            {
                PostInitialize();
                _stateController.stateTransition(GameState.DEMO, false);
            }
            // ...

        }

        /// <summary>
        /// Setup the game
        /// </summary>
        private void gameInitialize()
        {
            _gameplay.initializeGameAssets();
        }

        /// <summary>
        /// Save out all game data into an XML file
        /// </summary>
        private void saveGame()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";
            settings.NewLineOnAttributes = true;

            XmlWriter writer = XmlWriter.Create(_SAVEPATH, settings);

            writer.WriteStartElement("Game");

            writer.WriteElementString("NextID", _gameObjectManager.getNextIDForSave().ToString());
            writer.WriteElementString("GameState", _stateController.gameState.ToString());

            _gameplay.gameStats.save(writer);

            _gameObjectManager.saveObjects(writer);
            writer.WriteEndElement();
            writer.Close();
        }

        /// <summary>
        /// Load all game data and re-initialize everything
        /// </summary>
        private void loadGame()
        {
            // Clear old objects from registries
            _gameObjectManager.clearGameObjects();
            _renderManager.clearRenderers();
            _physicsManager.clearColliders();

            _gameplay.onGameLoaded();

            XmlDocument document = new XmlDocument();

            document.Load(_SAVEPATH);

            int nextID = int.Parse(document.SelectSingleNode("Game/NextID").InnerText);
            _gameObjectManager.nextID = nextID;
            GameState st = parseState(document.SelectSingleNode("Game/GameState").InnerText);

            _gameplay.gameStats.load(document.SelectSingleNode("Game/Stats"));

            // Recreate screen bounds
            _screenBounds = new ScreenBounds(this);

            XmlNode objectListNode = document.SelectSingleNode("Game/GameObjects");

            _gameObjectManager.loadObjects(objectListNode);

            _stateController.stateTransition(GameState.PLAY, true);
        }

        /// <summary>
        /// Parse the state from a string into a readable state
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private GameState parseState(string input)
        {
            switch (input)
            {
                case "DEMO":
                    return GameState.DEMO;
                case "INITIALIZE":
                    return GameState.INITIALIZE;
                case "PLAY":
                    return GameState.PLAY;
                case "TRANSITION":
                    return GameState.TRANSITION;
                default:
                    throw new Exception("Failed to get a valid state!");
            }
        }

    }
}
