﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Xml;

namespace PickUpTheCrew
{
    public class Highscores
    {
        private int _maxScores;
        private List<int> _scores = new List<int>();

        /// <summary>
        /// All the scores in the highscores
        /// </summary>
        public List<int> scores { get { return _scores; } }

        public Highscores(int maxScores)
        {
            _maxScores = maxScores;

            for (int i = 0; i < maxScores; i++)
            {
                _scores.Add(0);
            }
        }

        /// <summary>
        /// Submit a score to the highscores
        /// </summary>
        /// <param name="score"></param>
        /// <returns>Whether or not a highscore has been set</returns>
        public bool submitScore(int score, out int scoreIndex)
        {
            if (score > _scores[0]) // is it a highscore
            {
                for (int i = 0; i < _scores.Count - 1; i++)
                {
                    if (score >= _scores[i] && score <= _scores[i + 1]) // does score fit inbetween these 2?
                    {
                        for (int j = 1; j <= i; j++)
                        {
                            _scores[j - 1] = _scores[j];
                        }

                        scores[i] = score;
                        scoreIndex = i;
                        return true;
                    }
                }

                // score must be the end score to replace
                for (int i = 1; i < _scores.Count; i++)
                {
                    _scores[i - 1] = _scores[i];
                }

                _scores[_scores.Count - 1] = score;
                scoreIndex = _scores.Count - 1;
                return true;

            }
            else
            {
                scoreIndex = -1;
                return false;
            }
        }

        /// <summary>
        /// Save the highscores data
        /// </summary>
        /// <param name="writer"></param>
        public void save(XmlWriter writer)
        {
            writer.WriteStartElement("Scores");

            for (int i = 0; i < _scores.Count; i++)
            {
                writer.WriteElementString("Score", _scores[i].ToString());
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Load the highscores data
        /// </summary>
        /// <param name="node"></param>
        public void load(XmlNode node)
        {
            XmlNodeList nodes = node.SelectNodes("Score");

            List<int> sc = new List<int>();

            foreach (XmlNode n in nodes)
            {
                int s = int.Parse(n.InnerText);
                sc.Add(s);
            }

            _maxScores = sc.Count;
            _scores = sc;
        }

    }
}
