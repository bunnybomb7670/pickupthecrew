﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PickUpTheCrew
{
    public class AnimatorRenderer : Renderer
    {
        private Animator _animator;
        private Vector2 _tileDimensions;

        public AnimatorRenderer(Animator animator, Texture2D texture, Vector2 tileDimensions)
            : base((GameObject)animator, texture)
        {
            _tileDimensions = tileDimensions;
            _animator = animator;
        }

        public override void draw(SpriteBatch spriteBatch)
        {
            Vector2 pos = _animator.sheetPos;
            Vector2 p = _animator.transform.position;

            Rectangle srcRect = new Rectangle((int)(pos.X * _tileDimensions.X), (int)(pos.Y * _tileDimensions.Y), (int)_tileDimensions.X, (int)_tileDimensions.Y);

            for (int i = 0; i < _animator.repeatX; i++)
            {
                for (int j = 0; j < _animator.repeatY; j++)
                {
                    Vector2 nP = p + new Vector2(i * _tileDimensions.X * _gameObject.transform.scale, j * _tileDimensions.Y * _gameObject.transform.scale);
                    spriteBatch.Draw(texture, nP, srcRect, Color.White, 0.0f, Vector2.Zero, _gameObject.transform.scale, SpriteEffects.None, 1.0f);
                }
            }
        }
    }
}