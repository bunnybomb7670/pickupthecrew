﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna;
using Microsoft.Xna.Framework;

namespace PickUpTheCrew
{
    public struct Particle
    {
        private float _life;
        private float _maxLife;
        private float _rotation;
        private float _scale;
        private float _rotationDirection;
        private Vector2 _position;

        /// <summary>
        /// The "life" left for the particle
        /// </summary>
        public float life { get { return _life; } set { _life = value; } }

        /// <summary>
        /// The total "life" the particle
        /// </summary>
        public float maxLife { get { return _maxLife; } set { _maxLife = value; } }

        /// <summary>
        /// The current rotation of the particle
        /// </summary>
        public float rotation { get { return _rotation; } set { _rotation = value; } }

        /// <summary>
        /// The direction in which the particle is rotating
        /// </summary>
        public float rotationDirection { get { return _rotationDirection; } set { _rotationDirection = value; } }

        /// <summary>
        /// The scale of the particle
        /// </summary>
        public float scale { get { return _scale; } set { _scale = value; } }

        /// <summary>
        /// The 2D position of the particle
        /// </summary>
        public Vector2 position { get { return _position; } set { _position = value; } }

        public Particle(float life, float maxLife, Vector2 position, float rotation, float scale, Direction rotationDirection)
        {
            _life = life;
            _maxLife = maxLife;
            _position = position;
            _rotation = rotation;
            _rotationDirection = rotationDirection == Direction.LEFT ? 1.0f : -1.0f;
            _scale = scale;
        }

        /// <summary>
        /// The amount of life left in the particle
        /// </summary>
        /// <returns>A float percentage representing its life left</returns>
        public float getLifePercent()
        {
            return (_life / _maxLife) * 100.0f;
        }
    }
}