﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Xml;

namespace PickUpTheCrew
{
    public class ParticleRenderer : Renderer
    {
        private ParticleEmitter _emitter;

        public ParticleRenderer(GameObject gameObject, Texture2D texture)
            : base(gameObject, texture)
        {
            _emitter = (ParticleEmitter)gameObject;
        }

        public ParticleRenderer(GameObject gameObject, XmlNode node)
            : base(gameObject, node) { }

        public override void draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < _emitter.particles.Count; i++)
            {
                Particle p = _emitter.particles[i];
                Color c = _color;
                float pP = p.getLifePercent();

                // Smoothly adjust the alpha of the particle depending on the state of its life,
                // when over 75% life, it will inverse fade from 0 - 200 alpha to give a nice fade-in 
                // effect rather than just popping in and looking jumpy.

                c.A = pP > 75 ? (byte)(200.0f - ((pP - 75) * 8f)) : (byte)(2.00f * p.getLifePercent());

                spriteBatch.Draw(_texture, p.position, null, c, p.rotation, new Vector2(texture.Width / 2, texture.Height / 2), p.scale, SpriteEffects.None, _renderDepth + (0.0001f * i));
            }
        }
    }
}