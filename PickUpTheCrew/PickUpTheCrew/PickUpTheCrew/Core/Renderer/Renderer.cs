﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Xml;

namespace PickUpTheCrew
{
    public class Renderer
    {
        protected GameObject _gameObject;
        protected Texture2D _texture;
        protected Color _color;
        protected bool _enabled;
        protected bool _flip;
        protected float _renderDepth;

        public GameObject gameObject { get { return _gameObject; } }
        public Texture2D texture { get { return _texture; } set { _texture = value; } }
        public Color color { get { return _color; } set { _color = value; } }
        public bool enabled { get { return _enabled; } set { _enabled = value; } }
        public bool flip { get { return _flip; } set { _flip = value; } }
        public Vector2 textureBounds { get { return new Vector2(texture.Width, texture.Height); } }
        public Rectangle renderRectangle { get { return new Rectangle((int)_gameObject.transform.position.X, (int)_gameObject.transform.position.Y, _texture.Width / 2, _texture.Height / 2); } }
        public float renderDepth { get { return _renderDepth; } set { _renderDepth = value; } }

        public Renderer(GameObject gameObject, Texture2D texture)
        {
            _gameObject = gameObject;
            _texture = texture == null ? gameObject.game.renderManager.missingTexture : texture;
            _color = Color.White;
            _enabled = true;
            _renderDepth = 0.0f;
            gameObject.game.renderManager.registerRenderer(this);
        }

        public Renderer(GameObject gameObject, XmlNode node)
        {
            _gameObject = gameObject;
            _texture = TextureManager.getTexture(node.SelectSingleNode("Texture").InnerText);
            _color = IOUtils.parseColor(node.SelectSingleNode("Color").InnerText);
            _enabled = bool.Parse(node.SelectSingleNode("Enabled").InnerText);
            _flip = bool.Parse(node.SelectSingleNode("Flip").InnerText);
            _renderDepth = float.Parse(node.SelectSingleNode("RenderDepth").InnerText);

            gameObject.game.renderManager.registerRenderer(this);

            loadCustomData(node);
        }

        public void setTexture(Texture2D texture)
        {
            _texture = texture;
        }

        public void save(XmlWriter writer)
        {
            writer.WriteStartElement("Renderer");

            writer.WriteElementString("Type", this.GetType().FullName);
            writer.WriteElementString("Texture", _texture.Name);
            writer.WriteElementString("Color", _color.ToString());
            writer.WriteElementString("Enabled", _enabled.ToString());
            writer.WriteElementString("Flip", _flip.ToString());
            writer.WriteElementString("RenderDepth", _renderDepth.ToString());

            writer.WriteEndElement();

            saveCustomData(writer);
        }

        /// <summary>
        /// Default method to simply draw the object on the screen relative to its position rotation and scale.
        /// </summary>
        /// <param name="spriteBatch"></param>
        public virtual void draw(SpriteBatch spriteBatch)
        {
            //TODO : independant render depth
            spriteBatch.Draw(_texture, _gameObject.transform.position, null, _color, _gameObject.transform.rotation, new Vector2(_texture.Width / 2, _texture.Height / 2), _gameObject.transform.scale, flip ? SpriteEffects.FlipHorizontally : SpriteEffects.None, _renderDepth);
        }

        public virtual void saveCustomData(XmlWriter writer) { }

        public virtual void loadCustomData(XmlNode node) { }
    }
}