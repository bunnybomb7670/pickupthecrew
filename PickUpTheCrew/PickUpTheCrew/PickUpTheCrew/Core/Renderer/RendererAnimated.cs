﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Xml;

namespace PickUpTheCrew
{
    public class RendererAnimated : Renderer
    {
        protected int _sheetX, _sheetY, _endIndex;
        protected int _frameIndex;
        protected int _sheetSectionWidth;
        protected int _sheetSectionHeight;

        public int frameIndex { get { return _frameIndex; } set { _frameIndex = value; } }

        public RendererAnimated(GameObject gameObject, Texture2D texture, int sheetX, int sheetY, int endIndex)
            : base(gameObject, texture)
        {
            _sheetX = sheetX;
            _sheetY = sheetY;
            _endIndex = endIndex;

            _sheetSectionWidth = texture.Width / _sheetX;
            _sheetSectionHeight = texture.Height / _sheetY;
        }

        public RendererAnimated(GameObject gameObject, XmlNode node)
            : base(gameObject, node) { }

        /// <summary>
        /// Change to the next frame
        /// </summary>
        public void nextFrame()
        {
            if (_frameIndex > _sheetX * _sheetY || _frameIndex >= _endIndex)
            {
                _frameIndex = 0;
            }
            else
            {
                _frameIndex++;
            }
        }

        public override void draw(SpriteBatch spriteBatch)
        {
            int x = (int)_frameIndex % _sheetX;
            int y = (int)_frameIndex / _sheetX;
            Rectangle frameRect = new Rectangle(x * _sheetSectionWidth, y * _sheetSectionHeight, _sheetSectionWidth, _sheetSectionHeight);
            Vector2 textureOrigin = new Vector2(_texture.Width / 2, _texture.Height / 2);

            spriteBatch.Draw(_texture, _gameObject.transform.position, frameRect, _color, _gameObject.transform.rotation, textureOrigin, new Vector2(1, 1), SpriteEffects.None, 0.0f);
        }

        public override void loadCustomData(XmlNode node)
        {
            _sheetX = int.Parse(node.SelectSingleNode("SheetX").InnerText);
            _sheetY = int.Parse(node.SelectSingleNode("SheetY").InnerText);
            _endIndex = int.Parse(node.SelectSingleNode("EndIndex").InnerText);
            _frameIndex = int.Parse(node.SelectSingleNode("FraneIndex").InnerText);
        }

        public override void saveCustomData(XmlWriter writer)
        {
            writer.WriteElementString("SheetX", _sheetX.ToString());
            writer.WriteElementString("SheetY", _sheetY.ToString());
            writer.WriteElementString("EndIndex", _endIndex.ToString());
            writer.WriteElementString("FraneIndex", _frameIndex.ToString());
        }
    }
}