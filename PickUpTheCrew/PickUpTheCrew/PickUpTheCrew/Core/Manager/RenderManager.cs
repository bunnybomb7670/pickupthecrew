﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace PickUpTheCrew
{
    public class RenderManager
    {
        private MainGame _game;
        private SpriteBatch _spriteBatch;

        private List<Renderer> _renderers = new List<Renderer>();
        private List<Texture2D> _crewTextures = new List<Texture2D>();

        #region textures

        private Texture2D _missingTexture;
        private Texture2D _pointTexture;
        private Texture2D _lineTexture;

        private Texture2D _numberSheet;

        private SpriteFont _drawFont;
        private SpriteFont _displayFont;

        private Texture2D _titleTexture;
        private Texture2D _gameOverTexture;
        private Texture2D _keyPromptTexture;
        private Texture2D _keyPromotTextureContinue;
        private Texture2D _scoreTexture;
        private Texture2D _highscoreTexture;
        private Texture2D _levelTexture;
        private Texture2D _pickedUpTexture;
        private Texture2D _newScoreTexture;
        private Texture2D _arrowTexture;

        private Texture2D _crewNameBlue;
        private Texture2D _crewNameGreen;
        private Texture2D _crewNameYellow;
        private Texture2D _crewNameOrange;
        private Texture2D _crewNamePink;
        private Texture2D _crewNameRed;

        #endregion textures

        #region textureNames

        private string _DEFAULTFONTNAME = "defaultFont";
        private string _DISPLAYFONTNAME = "displayFont";

        private string _NUMBERSHEETNAME = "gameNumberSheet";

        private string _CREWBLUETEXTURENAME = "crewName_blue";
        private string _CREWGREENTEXTURENAME = "crewName_green";
        private string _CREWYELLOWTEXTURENAME = "crewName_yellow";
        private string _CREWORANGETEXTURENAME = "crewName_orange";
        private string _CREWPINKTEXTURENAME = "crewName_pink";
        private string _CREWREDTEXTURENAME = "crewName_red";

        private string _MISSINGTEXTURENAME = "missingTexture";
        private string _POINTTEXTURENAME = "cross";
        private string _TITLETEXTURENAME = "PickUpTheCrew_Title";
        private string _GAMEOVERTEXTURENAME = "PickUpTheCrew_GameOver";
        private string _KEYPROMPTTEXTURENAME = "PickUpTheCrew_Start";
        private string _KEYPROMPTTEXTURENAMECONTINUE = "PickUpTheCrew_Continue";
        private string _SCORETEXTURENAME = "PickUpTheCrew_Score";
        private string _HIGHSCORETEXTURENAME = "PickUpTheCrew_Highscores_2";
        private string _LEVELTEXTURENAME = "PickUpTheCrew_Level";
        private string _PICKEDUPTEXTURENAME = "PickUpTheCrew_PickedUp";
        private string _NEWSCORETEXTURE = "PickUpTheCrew_newScore";
        private string _ARROWTEXTURE = "PickUpTheCrew_arrow";

        #endregion textureNames

        /// <summary>
        /// The last collected crew type
        /// </summary>
        public CrewType lastCollectedCrewType;

        /// <summary>
        /// Whether or not the last crew type message is visible
        /// </summary>
        public bool lastCollectedCrewTypeVisible;

        private bool _debugDrawing;
        private bool _debugColliderDrawing;

        /// <summary>
        /// Game instance reference
        /// </summary>
        public MainGame game { get { return _game; } }

        /// <summary>
        /// Missing texture reference for NULL textures
        /// </summary>
        public Texture2D missingTexture { get { return _missingTexture; } }

        /// <summary>
        /// Should debug stats be drawn on the screen?
        /// </summary>
        public bool debugDrawing { get { return _debugDrawing; } set { _debugDrawing = value; } }

        /// <summary>
        /// Should collider bounds be drawn on the screen?
        /// </summary>
        public bool debugColliderDrawing { get { return _debugColliderDrawing; } set { _debugColliderDrawing = value; } }

        public RenderManager(MainGame game, GraphicsDevice device)
        {
            _game = game;
            _spriteBatch = new SpriteBatch(device);

            // Assign default textures to be used
            _missingTexture = TextureManager.getTexture(_MISSINGTEXTURENAME);
            _pointTexture = TextureManager.getTexture(_POINTTEXTURENAME);

            _titleTexture = TextureManager.getTexture(_TITLETEXTURENAME);
            _gameOverTexture = TextureManager.getTexture(_GAMEOVERTEXTURENAME);
            _keyPromptTexture = TextureManager.getTexture(_KEYPROMPTTEXTURENAME);
            _keyPromotTextureContinue = TextureManager.getTexture(_KEYPROMPTTEXTURENAMECONTINUE);
            _scoreTexture = TextureManager.getTexture(_SCORETEXTURENAME);
            _highscoreTexture = TextureManager.getTexture(_HIGHSCORETEXTURENAME);
            _numberSheet = TextureManager.getTexture(_NUMBERSHEETNAME);
            _levelTexture = TextureManager.getTexture(_LEVELTEXTURENAME);
            _pickedUpTexture = TextureManager.getTexture(_PICKEDUPTEXTURENAME);
            _newScoreTexture = TextureManager.getTexture(_NEWSCORETEXTURE);
            _arrowTexture = TextureManager.getTexture(_ARROWTEXTURE);

            _crewNameBlue = TextureManager.getTexture(_CREWBLUETEXTURENAME);
            _crewNameGreen = TextureManager.getTexture(_CREWGREENTEXTURENAME);
            _crewNameYellow = TextureManager.getTexture(_CREWYELLOWTEXTURENAME);
            _crewNameOrange = TextureManager.getTexture(_CREWORANGETEXTURENAME);
            _crewNamePink = TextureManager.getTexture(_CREWPINKTEXTURENAME);
            _crewNameRed = TextureManager.getTexture(_CREWREDTEXTURENAME);

            // load crew textures
            for (int i = 0; i < 6; i++)
            {
                _crewTextures.Add(getCrewTexture(i));
            }

            // Generate a 1x1 texture to be used for drawing lines on the screen
            _lineTexture = new Texture2D(device, 1, 1);
            _lineTexture.SetData(new Color[] { Color.White });

            // Load default drawing font
            _drawFont = FontManager.getFont(_DEFAULTFONTNAME);
            _displayFont = FontManager.getFont(_DISPLAYFONTNAME);
        }

        /// <summary>
        /// Get the texture file for the crew at the specified index
        /// </summary>
        /// <param name="index"></param>
        /// <returns>A texture linked to the crew index</returns>
        private Texture2D getCrewTexture(int index)
        {
            string name = "";
            switch (index)
            {
                case 0:
                    name = "crewBlue";
                    break;
                case 1:
                    name = "crewGreen";
                    break;
                case 2:
                    name = "crewRed";
                    break;
                case 3:
                    name = "crewOrange";
                    break;
                case 4:
                    name = "crewPink";
                    break;
                case 5:
                    name = "crewYellow";
                    break;
                default: throw new Exception("Failed to get crew texture, invalid index specified.");
            }

            Texture2D texture = TextureManager.getTexture(name);
            return texture;
        }

        /// <summary>
        /// Get a crew textur from a crewType reference
        /// </summary>
        /// <param name="crewType"></param>
        /// <returns>A texture related to a crewType</returns>
        public Texture2D getTextureForCrew(CrewType crewType)
        {
            switch (crewType)
            {
                case CrewType.Lieutenant_Sky:
                    return _crewTextures[0];
                case CrewType.Midshipman_Verde:
                    return _crewTextures[1];
                case CrewType.Cabinboy_Crimson:
                    return _crewTextures[2];
                case CrewType.Stoker_Orange:
                    return _crewTextures[3];
                case CrewType.Able_Seaman_Shocking_Pink:
                    return _crewTextures[4];
                case CrewType.Commander_Custard:
                    return _crewTextures[5];
                default: throw new Exception("Failed to get texture for crew type specified");
            }
        }

        /// <summary>
        /// Register a renderer to the list
        /// </summary>
        /// <param name="renderer"></param>
        public void registerRenderer(Renderer renderer)
        {
            _renderers.Add(renderer);
        }

        /// <summary>
        /// Un-register a renderer from the list
        /// </summary>
        /// <param name="renderer"></param>
        public void unregisterRenderer(Renderer renderer)
        {
            _renderers.Remove(renderer);
        }

        /// <summary>
        /// Remove all renderer references
        /// </summary>
        public void clearRenderers()
        {
            for (int i = _renderers.Count - 1; i >= 0; i--)
            {
                Renderer r = _renderers[i];
                if (r.gameObject.destroyOnLoad)
                {
                    _renderers.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Construct and Draw a number on the screen from a tile based texture sheet
        /// </summary>
        /// <param name="number"></param>
        /// <param name="position"></param>
        /// <param name="scale"></param>
        /// <param name="depth"></param>
        public void drawNumber(int number, Vector2 position, float scale, float depth)
        {
            int width = _numberSheet.Width;
            int height = _numberSheet.Height;

            int rows = 4;

            float characterOffset = 64 * scale;

            int tileSize = width / rows;

            string num = number.ToString();

            for (int i = 0; i < num.Length; i++)
            {
                int sub = int.Parse(num[i].ToString());

                int x = sub % rows;
                int y = sub / rows;

                Rectangle src = new Rectangle(x * tileSize, y * tileSize, tileSize, tileSize);

                _spriteBatch.Draw(_numberSheet, position + new Vector2(characterOffset * i, 0), src, Color.White, 0.0f, new Vector2(width / 2, height / 2), new Vector2(scale, scale), SpriteEffects.None, depth);
            }
        }

        /// <summary>
        /// Draw a string on the screen
        /// </summary>
        /// <param name="text"></param>
        /// <param name="position"></param>
        /// <param name="color"></param>
        public void drawString(object text, Vector2 position, Color color)
        {
            _spriteBatch.DrawString(_drawFont, text.ToString(), position, color);
        }

        /// <summary>
        /// Draw a string on the screen with a specific font
        /// </summary>
        /// <param name="font"></param>
        /// <param name="text"></param>
        /// <param name="position"></param>
        /// <param name="color"></param>
        public void drawString(SpriteFont font, object text, Vector2 position, Color color)
        {
            _spriteBatch.DrawString(font, text.ToString(), position, color);
        }

        /// <summary>
        /// Draw a point on the screen (as a cross)
        /// </summary>
        /// <param name="position"></param>
        /// <param name="color"></param>
        public void drawPoint(Vector2 position, Color color)
        {
            _spriteBatch.Draw(_pointTexture, position, null, color, 0.0f, new Vector2(_pointTexture.Width / 2, _pointTexture.Height / 2), new Vector2(1, 1), SpriteEffects.None, 0.0f);
        }

        /// <summary>
        /// Draw a line on the screen between point A and B
        /// </summary>
        /// <param name="positionA"></param>
        /// <param name="positionB"></param>
        /// <param name="color"></param>
        /// <param name="width"></param>
        /// <param name="renderDepth"></param>
        public void drawLine(Vector2 positionA, Vector2 positionB, Color color, float width, float renderDepth)
        {
            float angle = (float)Math.Atan2(positionB.Y - positionA.Y, positionB.X - positionA.X);
            float length = Vector2.Distance(positionA, positionB);

            _spriteBatch.Draw(_lineTexture, positionA, null, color, angle, Vector2.Zero, new Vector2(length, width), SpriteEffects.None, renderDepth);
        }

        /// <summary>
        /// Draw the bounds of an object
        /// </summary>
        /// <param name="bounds"></param>
        /// <param name="obj"></param>
        public void drawObjectBounds(Bounds bounds, GameObject obj)
        {
            Vector2 topLeft = new Vector2(bounds.minX, bounds.maxY);
            Vector2 topRight = new Vector2(bounds.maxX, bounds.maxY);
            Vector2 bottomLeft = new Vector2(bounds.minX, bounds.minY);
            Vector2 bottomRight = new Vector2(bounds.maxX, bounds.minY);

            drawLine(topLeft, bottomLeft, Color.Red, 1, 0.0f);
            drawLine(bottomLeft, bottomRight, Color.Red, 1, 0.0f);
            drawLine(bottomRight, topRight, Color.Red, 1, 0.0f);
            drawLine(topRight, topLeft, Color.Red, 1, 0.0f);
        }

        /// <summary>
        /// Draw all objects in the renderer list
        /// </summary>
        /// <param name="gameTime"></param>
        public void draw(GameTime gameTime)
        {
            _spriteBatch.GraphicsDevice.Clear(new Color(28, 107, 160, 255));
            _spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied, SamplerState.LinearWrap, DepthStencilState.Default, RasterizerState.CullCounterClockwise, null);

            for (int i = 0; i < _renderers.Count; i++)
            {
                _renderers[i].gameObject.onDraw(_spriteBatch);
                if (_renderers[i].enabled)
                {
                    _renderers[i].draw(_spriteBatch);
                }
            }

            _spriteBatch.End();
        }

        /// <summary>
        /// Draw all game info on screen
        /// </summary>
        public void drawDebugData(GameTime gameTime)
        {
            // draw info
            float fps = _game.fpsCalculator.fps;
            float mod = 1 - (fps / 60.0f);

            float colorValue = MathHelper.Lerp(0, 1, mod);
            Color fpsColor = Color.Lerp(Color.Lime, Color.Orange, mod);

            Color drawColor = Color.White;
            Color outlineColor = Color.Black;
            float outlineAlphaMod = 0.3f;

            // draw debug info
            drawOutlinedString(_drawFont, "FPS : " + (int)_game.fpsCalculator.fps, new Vector2(5, 0), fpsColor, fpsColor * outlineAlphaMod);
            drawOutlinedString(_drawFont, "Textures : " + TextureManager.loadedTextures, new Vector2(5, 25), drawColor, outlineColor * outlineAlphaMod);
            drawOutlinedString(_drawFont, "Fonts : " + FontManager.loadedFonts, new Vector2(5, 50), drawColor, outlineColor * outlineAlphaMod);
            drawOutlinedString(_drawFont, "GameObjects : " + _game.gameObjectManager.totalGameObjects, new Vector2(5, 75), drawColor, outlineColor * outlineAlphaMod);
            drawOutlinedString(_drawFont, "Pooled Crew : " + _game.gameObjectManager.pooledCrew, new Vector2(5, 100), drawColor, outlineColor * outlineAlphaMod);
            drawOutlinedString(_drawFont, "Pooled Sharks : " + _game.gameObjectManager.pooledSharks, new Vector2(5, 125), drawColor, outlineColor * outlineAlphaMod);
            drawOutlinedString(_drawFont, "Renderers : " + _renderers.Count, new Vector2(5, 150), drawColor, outlineColor * outlineAlphaMod);
            drawOutlinedString(_drawFont, "Particles : " + ParticleEmitter.TOTAL_PARTICLE_COUNT, new Vector2(5, 175), drawColor, outlineColor * outlineAlphaMod);
            drawOutlinedString(_drawFont, "Colliders : " + game.physicsManager.colliderCount, new Vector2(5, 200), drawColor, outlineColor * outlineAlphaMod);
            drawOutlinedString(_drawFont, "Collision Checks : " + game.physicsManager.collisionChecks, new Vector2(5, 225), drawColor, outlineColor * outlineAlphaMod);
            drawOutlinedString(_drawFont, "Collision Intersections : " + game.physicsManager.collisionIntersections, new Vector2(5, 250), drawColor, outlineColor * outlineAlphaMod);
            drawOutlinedString(_drawFont, "GameState : " + game.stateController.gameState, new Vector2(5, 275), drawColor, outlineColor * outlineAlphaMod);
            drawOutlinedString(_drawFont, "Demo Sharks : " + game.gameplay.demoSharkCount, new Vector2(5, 300), drawColor, outlineColor * outlineAlphaMod);

            // draw mouse pos info
            MouseState state = Mouse.GetState();
            int x = state.X;
            int y = state.Y;

            drawOutlinedString(_drawFont, "Mouse pos : " + new Vector2(x, y), new Vector2(5, 325), drawColor, outlineColor * outlineAlphaMod);

            drawOutlinedString(_drawFont, "Gameplay Crew : " + game.gameplay._crew.Count, new Vector2(5, 350), drawColor, outlineColor * outlineAlphaMod);
            drawOutlinedString(_drawFont, "Gameplay Sharks : " + game.gameplay._sharks.Count, new Vector2(5, 375), drawColor, outlineColor * outlineAlphaMod);

        }

        /// <summary>
        /// Draw a string with an outline
        /// </summary>
        /// <param name="font"></param>
        /// <param name="text"></param>
        /// <param name="position"></param>
        /// <param name="mainColor"></param>
        /// <param name="outlineColor"></param>
        public void drawOutlinedString(SpriteFont font, object text, Vector2 position, Color mainColor, Color outlineColor)
        {
            // Draw outline
            _spriteBatch.DrawString(font, text.ToString(), position + new Vector2(0, 1), outlineColor, 0.0f, Vector2.Zero, new Vector2(1, 1), SpriteEffects.None, 0.1f);
            _spriteBatch.DrawString(font, text.ToString(), position + new Vector2(0, -1), outlineColor, 0.0f, Vector2.Zero, new Vector2(1, 1), SpriteEffects.None, 0.1f);
            _spriteBatch.DrawString(font, text.ToString(), position + new Vector2(1, 0), outlineColor, 0.0f, Vector2.Zero, new Vector2(1, 1), SpriteEffects.None, 0.1f);
            _spriteBatch.DrawString(font, text.ToString(), position + new Vector2(-1, 0), outlineColor, 0.0f, Vector2.Zero, new Vector2(1, 1), SpriteEffects.None, 0.1f);

            // Draw main text
            _spriteBatch.DrawString(font, text.ToString(), position, mainColor, 0.0f, Vector2.Zero, new Vector2(1, 1), SpriteEffects.None, 0.0f);
        }

        /// <summary>
        /// Draw the UI ( score etc...)
        /// </summary>
        public void drawUI(GameTime gameTime, GameState state)
        {
            _spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied, SamplerState.PointClamp, DepthStencilState.Default, RasterizerState.CullCounterClockwise, null);

            switch (state)
            {
                case GameState.DEMO:

                    drawDemoUI(gameTime);

                    break;

                case GameState.PLAY:

                    drawGameUI(gameTime);

                    break;

                case GameState.TRANSITION:

                    drawTransitionUI(gameTime);

                    break;
            }

            _spriteBatch.End();
        }

        /// <summary>
        /// Draw the games UI
        /// </summary>
        /// <param name="gameTime"></param>
        void drawGameUI(GameTime gameTime)
        {
            if (_debugDrawing)
            {
                drawDebugData(gameTime);
            }

            Vector2 bounds = _game.windowBounds;
            Vector2 mid = new Vector2(bounds.X / 2, bounds.Y / 2);

            int level = _game.gameplay.gameStats.level;
            int score = _game.gameplay.gameStats.score;

            // draw score
            _spriteBatch.Draw(_scoreTexture, new Vector2(1800, 104), null, Color.White, 0.0f, new Vector2(_levelTexture.Width, _levelTexture.Height), 0.5f, SpriteEffects.None, 0.0f);
            drawNumber(score, new Vector2(1735, 108), 0.4f, 0.0f);

            // draw level
            _spriteBatch.Draw(_levelTexture, new Vector2(350, 104), null, Color.White, 0.0f, new Vector2(_levelTexture.Width, _levelTexture.Height), 0.5f, SpriteEffects.None, 0.0f);
            drawNumber(level, new Vector2(285, 108), 0.4f, 0.0f);

            // draw crew pickup display

            if (lastCollectedCrewTypeVisible)
            {
                Texture2D tex;

                switch (lastCollectedCrewType)
                {
                    case CrewType.Able_Seaman_Shocking_Pink:
                        tex = _crewNamePink;
                        break;
                    case CrewType.Cabinboy_Crimson:
                        tex = _crewNameRed;
                        break;
                    case CrewType.Commander_Custard:
                        tex = _crewNameYellow;
                        break;
                    case CrewType.Lieutenant_Sky:
                        tex = _crewNameBlue;
                        break;
                    case CrewType.Midshipman_Verde:
                        tex = _crewNameGreen;
                        break;
                    case CrewType.Stoker_Orange:
                        tex = _crewNameOrange;
                        break;
                    default: throw new Exception("Failed to get crew name type");
                }

                Color alph = Color.White * (_game.gameplay.nameFadePercent / 100.0f);

                Vector2 sizeT = new Vector2(tex.Width / 2, tex.Height / 2);
                _spriteBatch.Draw(tex, new Vector2(mid.X + (sizeT.X / 2), 154), null, alph, 0.0f, new Vector2(_levelTexture.Width, _levelTexture.Height), 0.5f, SpriteEffects.None, 0.0f);
                _spriteBatch.Draw(_pickedUpTexture, new Vector2(mid.X + (sizeT.X / 2), 104), null, alph, 0.0f, new Vector2(_levelTexture.Width, _levelTexture.Height), 0.5f, SpriteEffects.None, 0.0f);
            }

        }

        /// <summary>
        /// Draw the attract mode UI
        /// </summary>
        /// <param name="gameTime"></param>
        void drawDemoUI(GameTime gameTime)
        {
            if (_debugDrawing)
            {
                drawDebugData(gameTime);
            }

            Vector2 bounds = _game.windowBounds;
            Vector2 mid = new Vector2(bounds.X / 2, bounds.Y / 2);

            Vector2 sizeT = new Vector2(_titleTexture.Width, _titleTexture.Height);
            Vector2 sizeS = new Vector2(_keyPromptTexture.Width, _keyPromptTexture.Height);

            float sin = (float)Math.Sin(_game.gameplay.titleSinWaveValue);
            float sinMod = 10.0f;

            Vector2 modSin = new Vector2(0, sinMod * sin);

            _spriteBatch.Draw(_titleTexture, new Vector2(mid.X - (sizeT.X / 2), -20) + modSin, Color.White);
            _spriteBatch.Draw(_keyPromptTexture, new Vector2(mid.X - (sizeS.X / 2), bounds.Y - 350), Color.White);

            // Draw highscores

            List<int> scores = _game.gameplay.highscores.scores;
            int c = scores.Count + 1;

            for (int i = 0; i < scores.Count; i++)
            {
                drawNumber(c - (i + 1), new Vector2(mid.X - 80, 620 + (i * 50)), 0.5f, 0.0f);
                drawNumber(scores[i], new Vector2(mid.X + 164, 620 + (i * 50)), 0.5f, 0.0f);
                if (game.gameplay.newHighscore && i == _game.gameplay.newHighscoreIndex)
                {
                    float sinI = (float)Math.Sin(_game.gameplay.titleSinWaveValue);
                    float sinModI = 10.0f;

                    Vector2 modSinI = new Vector2(sinModI * sinI, 0);

                    Vector2 sizeA = new Vector2(_arrowTexture.Width, _arrowTexture.Height);
                    _spriteBatch.Draw(_arrowTexture, new Vector2(mid.X - (sizeA.X / 2) - 290, 375 + (i * 50)) + modSinI, Color.White);
                }
            }

            Vector2 sizeSc = new Vector2(_highscoreTexture.Width, _highscoreTexture.Height);
            _spriteBatch.Draw(_highscoreTexture, new Vector2(mid.X - (sizeSc.X / 2), 260), Color.White);

            if (game.gameplay.newHighscore)
            {
                Vector2 sizeSs = new Vector2(_newScoreTexture.Width, _newScoreTexture.Height);
                _spriteBatch.Draw(_newScoreTexture, new Vector2(mid.X - (sizeSs.X / 2), 160), Color.White);
            }
        }

        /// <summary>
        /// Draw the transition (game over) UI
        /// </summary>
        /// <param name="gameTime"></param>
        void drawTransitionUI(GameTime gameTime)
        {
            if (_debugDrawing)
            {
                drawDebugData(gameTime);
            }

            Vector2 bounds = _game.windowBounds;
            Vector2 mid = new Vector2(bounds.X / 2, bounds.Y / 2);

            Vector2 size = new Vector2(_gameOverTexture.Width, _gameOverTexture.Height);

            Color alph = Color.White * (_game.gameplay.transitionTimePercent / 100.0f);

            _spriteBatch.Draw(_gameOverTexture, new Vector2(mid.X - (size.X / 2), 100), alph);
        }

    }
}