﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using System.IO;

namespace PickUpTheCrew
{
    public static class TextureManager
    {
        private static Dictionary<string, Texture2D> _textures = new Dictionary<string, Texture2D>();

        /// <summary>
        /// The total amount of textures loaded
        /// </summary>
        public static int loadedTextures { get { return _textures.Count; } }

        /// <summary>
        /// Load all textures from the resources
        /// </summary>
        /// <param name="content"></param>
        /// <param name="path"></param>
        public static void loadTextures(ContentManager content, string path)
        {
            string folderPath = content.RootDirectory + "/" + path;

            DirectoryInfo pathInfo = new DirectoryInfo(folderPath);

            if (!pathInfo.Exists)
            {
                throw new DirectoryNotFoundException("Unable to find Texture Directory (" + folderPath + ")");
            }

            FileInfo[] files = pathInfo.GetFiles();

            foreach (FileInfo file in files)
            {
                string p = Path.GetFileNameWithoutExtension(file.Name);

                Texture2D t = content.Load<Texture2D>(path + "/" + p);
                t.Name = p;
                _textures.Add(p, t);
            }
        }

        /// <summary>
        /// Get a specific texture with a name from the list
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        public static Texture2D getTexture(string texture)
        {
            Texture2D t = _textures[texture];
            if (t == null)
            {
                throw new Exception("Unable to find texture '" + _textures + "'.");
            }
            return t;
        }
    }
}