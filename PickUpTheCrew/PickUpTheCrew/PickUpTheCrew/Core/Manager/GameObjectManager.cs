﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using System.Xml;

namespace PickUpTheCrew
{
    public class GameObjectManager
    {
        private MainGame _game;
        private Dictionary<int, GameObject> _gameObjects = new Dictionary<int, GameObject>();
        private List<GameObject> _iterationList = new List<GameObject>();
        private int _nextID = 0;

        // Object pools
        private Pool<Shark> sharkPool;
        private Pool<Crew> crewPool;

        // GameObject storage
        public Dictionary<int, GameObject> gameObjects { get { return _gameObjects; } }
        public List<GameObject> iterationList { get { return _iterationList; } }

        // The next available unique ID
        public int nextID { get { return _nextID; } set { _nextID = value; } }

        /// <summary>
        /// The total amount of GameObjects
        /// </summary>
        public int totalGameObjects { get { return _iterationList.Count; } }

        /// <summary>
        /// The total amount of pooled sharks
        /// </summary>
        public int pooledSharks { get { return sharkPool.count; } }

        /// <summary>
        /// The total amount of pooled crew
        /// </summary>
        public int pooledCrew { get { return crewPool.count; } }

        public GameObjectManager(MainGame game, int startID)
        {
            _game = game;
            _nextID = startID;

            sharkPool = new Pool<Shark>(new InstancePoolItem<Shark>(createShark));
            crewPool = new Pool<Crew>(new InstancePoolItem<Crew>(createCrew));
        }

        /// <summary>
        /// Create an instance of a crew, this method is used for delegate behaviour in the object pools
        /// </summary>
        /// <returns>A new crew instance</returns>
        private Crew createCrew()
        {
            Shape crewShape = new Shape(new Vector2[] { new Vector2(-1, -1), new Vector2(-1, 1), new Vector2(1, 1), new Vector2(1, -1) }, 10);
            return new Crew(_game, TextureManager.getTexture("ringRed"), crewShape, CrewType.Able_Seaman_Shocking_Pink);
        }

        /// <summary>
        /// Create an instance of a crew, this method is used for delegate behaviour in the object pools
        /// </summary>
        /// <returns>A new shark instance</returns>
        private Shark createShark()
        {
            Shape s = new Shape(new Vector2[] { new Vector2(-32, 0), new Vector2(-16, -12), new Vector2(16, -12), new Vector2(32, 0), new Vector2(16, 10), new Vector2(-16, 10) });

            return new Shark(_game, TextureManager.getTexture("sharkTransparent"), s, null, 10, new Vector2(0, 0), false);
        }

        /// <summary>
        /// Get a shark from the pool
        /// </summary>
        /// <returns>A shark instance from the pool</returns>
        public Shark getPooledShark()
        {
            Shark shark = sharkPool.getItem();
            return shark;
        }

        /// <summary>
        /// Put a shark into the pool
        /// </summary>
        /// <param name="shark"></param>
        public void storeSharkInPool(Shark shark)
        {
            sharkPool.storeItem(shark);
        }

        /// <summary>
        /// Get a crew from the pool
        /// </summary>
        /// <returns>A crew instance from the pool</returns>
        public Crew getPooledCrew()
        {
            Crew crew = crewPool.getItem();
            return crew;
        }

        /// <summary>
        /// Put a crew into the pool
        /// </summary>
        /// <param name="crew"></param>
        public void storeCrewInPool(Crew crew)
        {
            crewPool.storeItem(crew);
        }

        /// <summary>
        /// Get the "NextID" value to be saved in the save data.
        /// </summary>
        /// <returns>The next available Unique ID value</returns>
        public int getNextIDForSave()
        {
            return _nextID;
        }

        /// <summary>
        /// Save all objects to an XML file
        /// </summary>
        /// <param name="writer"></param>
        public void saveObjects(XmlWriter writer)
        {
            writer.WriteStartElement("GameObjects");

            foreach (GameObject obj in _gameObjects.Values)
            {
                // Is the object persistent (should it be saved)?
                if (obj.persistent)
                {
                    obj.save(writer);
                }
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Load all objects from the XML data
        /// </summary>
        /// <param name="node"></param>
        public void loadObjects(XmlNode node)
        {
            XmlNodeList gameObjects = node.SelectNodes("GameObject");

            Dictionary<XmlNode, GameObject> objs = new Dictionary<XmlNode, GameObject>();

            List<Shark> s = new List<Shark>();
            List<Crew> c = new List<Crew>();

            foreach (XmlNode n in gameObjects)
            {
                GameObject o = IOUtils.createGameObjectInstanceFromNode(_game, n);
                objs.Add(n, o);

                string typeName = o.GetType().Name;

                switch (typeName)
                {
                    case "Shark":

                        s.Add((Shark)o);
                        break;
                    case "Crew":


                        c.Add((Crew)o);
                        break;
                }
            }

            foreach (KeyValuePair<XmlNode, GameObject> pair in objs)
            {
                pair.Value.postLoad(pair.Key);
            }

            _game.gameplay.setCrew(c);
            _game.gameplay.setSharks(s);

        }

        /// <summary>
        /// Iterate through the object list and update everything
        /// </summary>
        /// <param name="gameTime"></param>
        public void update(GameTime gameTime)
        {
            for (int i = 0; i < _iterationList.Count; i++)
            {
                if (_iterationList[i].enabled) // if the object is enabled
                {
                    _iterationList[i].update(gameTime);
                }
            }
        }

        /// <summary>
        /// Register a gameobject to the list with an auto-generated ID value
        /// </summary>
        /// <param name="gameObject"></param>
        /// <param name="objID"></param>
        public void registerGameObject(GameObject gameObject, out int objID)
        {
            int id = _nextID++;
            objID = id;
            _gameObjects.Add(id, gameObject);
            _iterationList.Add(gameObject);
        }

        /// <summary>
        /// Register a gameobject to the list with a pre-assigned ID value
        /// </summary>
        /// <param name="gameObject"></param>
        /// <param name="objID"></param>
        public void registerGameObject(GameObject gameObject, int objID)
        {
            _gameObjects.Add(objID, gameObject);
            _iterationList.Add(gameObject);
        }

        /// <summary>
        /// Un-register a gameobject from the list
        /// </summary>
        /// <param name="gameObject"></param>
        public void unregisterGameObject(GameObject gameObject)
        {
            _gameObjects.Remove(gameObject.ID);
            _iterationList.Remove(gameObject);
        }

        /// <summary>
        /// Get a GameObject by its unique ID
        /// </summary>
        /// <param name="objID"></param>
        /// <returns>A GameObject with the ID specified</returns>
        public GameObject getObject(int objID)
        {
            if (!_gameObjects.ContainsKey(objID))
            {
                throw new Exception("Unabled to find gameobject in registry");
            }

            GameObject o = _gameObjects[objID];
            return o;
        }

        /// <summary>
        /// Remove all GameObject references
        /// </summary>
        public void clearGameObjects()
        {
            sharkPool.clearPool();
            crewPool.clearPool();

            int[] ids = _gameObjects.Keys.ToArray();
            int c = 0;
            int t = _gameObjects.Count;

            for (int i = 0; i < ids.Length; i++)
            {
                if (_gameObjects.ContainsKey(ids[i]))
                {
                    GameObject o = _gameObjects[ids[i]];

                    if (o.destroyOnLoad)
                    {
                        o.onUnload();

                        _gameObjects.Remove(ids[i]);
                        _iterationList.Remove(o);
                        c++;
                    }
                }
            }
        }
    }
}