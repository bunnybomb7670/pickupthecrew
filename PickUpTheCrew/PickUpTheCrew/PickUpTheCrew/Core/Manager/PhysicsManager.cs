﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace PickUpTheCrew
{
    public class PhysicsManager
    {
        private List<PhysicsObject> _colliders = new List<PhysicsObject>();
        private MainGame _game;

        private int _collisionChecks = 0;
        private int _collisionIntersections = 0;

        /// <summary>
        /// The total amount of collision checks from the last frame
        /// </summary>
        public int collisionChecks { get { return _collisionChecks; } }

        /// <summary>
        /// The total amount of collision intersections from the last frame
        /// </summary>
        public int collisionIntersections { get { return _collisionIntersections; } }

        /// <summary>
        /// The total amount of colliders spawned
        /// </summary>
        public int colliderCount { get { return _colliders.Count; } }

        /// <summary>
        /// A reference to the main game instance
        /// </summary>
        public MainGame game { get { return _game; } }

        public PhysicsManager(MainGame game)
        {
            _game = game;
        }

        /// <summary>
        /// Register a collider for collision updates
        /// </summary>
        /// <param name="physicsObject"></param>
        public void registerCollider(PhysicsObject physicsObject)
        {
            _colliders.Add(physicsObject);
        }

        /// <summary>
        /// Remove all collider references
        /// </summary>
        public void clearColliders()
        {
            for (int i = _colliders.Count - 1; i >= 0; i-- )
            {
                PhysicsObject o = _colliders[i];
                if(o.destroyOnLoad)
                {
                    _colliders.RemoveAt(i);
                }
            }
        }

        /// <summary>
        /// Manage collision for all physics objects
        /// </summary>
        public void updatePhysics()
        {
            _collisionChecks = 0;
            _collisionIntersections = 0;

            for (int i = 0; i < _colliders.Count; i++)
            {
                if (_colliders[i].enabled)
                {
                    collisionCheck(i);
                }
            }
        }

        /// <summary>
        /// Manage the collision for a physics object
        /// </summary>
        /// <param name="index"></param>
        private void collisionCheck(int index)
        {
            PhysicsObject objA = _colliders[index];

            for (int i = index + 1; i < _colliders.Count; i++)
            {
                PhysicsObject objB = _colliders[i];

                if(!objB.enabled) // If object is not enabled, no collision interraction should occur
                {
                    continue;
                }

                if (canCollidersCollide(objA, objB))
                {
                    Vector2 resultantForce;
                    _collisionChecks++;

                    // Check for a physics intersection
                    if (objA.shape.checkIntersection(objA, objB, out resultantForce))
                    {
                        _collisionIntersections++;

                        // Should the collider be pushable? (force object out of intersection)
                        if (!objA.colliderStatic && !objB.trigger && !objA.trigger)
                        {
                            objA.transform.position += resultantForce;
                        }

                        // Is the other collider pushable? (force transferral to push object back)
                        if (!objB.colliderStatic && !objB.trigger && !objA.trigger)
                        {
                            // If the collider is static, it should not force away, it just straight off limit movement.
                            if (objA.colliderStatic)
                            {
                                objB.transform.position -= resultantForce;
                            }
                            else // collider is not static, add a bounce like reaction
                            {
                                objB.addForce(-resultantForce);
                            }
                        }

                        // Collision callback
                        objA.onCollision(objB);
                        objB.onCollision(objA);
                    }
                }
            }
        }

        /// <summary>
        /// Predetermine whether or not the objects are close enough to ever collide
        /// </summary>
        /// <returns>If the colliders can possibly collide</returns>
        private bool canCollidersCollide(PhysicsObject a, PhysicsObject b)
        {
            Bounds boundsA = a.shape.getBounds(a);
            Bounds boundsB = b.shape.getBounds(b);

            return boundsA.intersects(boundsB);
        }
    }
}