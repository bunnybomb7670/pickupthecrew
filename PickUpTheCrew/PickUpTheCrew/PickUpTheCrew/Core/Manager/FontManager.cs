﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using System.IO;

namespace PickUpTheCrew
{
    public class FontManager
    {
        private static Dictionary<string, SpriteFont> _fonts = new Dictionary<string, SpriteFont>();

        /// <summary>
        /// The amount of fonts loaded in the registry
        /// </summary>
        public static int loadedFonts { get { return _fonts.Count; } }

        /// <summary>
        /// Load all fonts from the resources
        /// </summary>
        /// <param name="content"></param>
        /// <param name="path"></param>
        public static void loadFonts(ContentManager content, string path)
        {
            string folderPath = content.RootDirectory + "/" + path;

            DirectoryInfo pathInfo = new DirectoryInfo(folderPath);

            if (!pathInfo.Exists)
            {
                throw new DirectoryNotFoundException("Unable to find Font Directory (" + folderPath + ")");
            }

            FileInfo[] files = pathInfo.GetFiles();

            foreach (FileInfo file in files)
            {
                string p = Path.GetFileNameWithoutExtension(file.Name);

                SpriteFont f = content.Load<SpriteFont>(path + "/" + p);
                _fonts.Add(p, f);
            }
        }

        /// <summary>
        /// Get a font from the registry
        /// </summary>
        /// <param name="font"></param>
        /// <returns>A specified font</returns>
        public static SpriteFont getFont(string font)
        {
            SpriteFont f = _fonts[font];
            if (f == null)
            {
                throw new Exception("Unable to find font '" + _fonts + "'.");
            }
            return f;
        }


    }
}
