﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using System.Xml;

namespace PickUpTheCrew
{
    /// <summary>
    /// A delegate used by the pool to allow creation of objects to be managed by the pool object itself.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public delegate T InstancePoolItem<T>();

    /// <summary>
    /// Generic pool class to manage the creation and store any un-used items for re-use in the future.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Pool<T> where T : GameObject, IPoolableObject
    {
        private List<T> _items = new List<T>();
        private InstancePoolItem<T> _createItemDelegate;

        public int count { get { return _items.Count; } }

        public Pool(InstancePoolItem<T> item)
        {
            _createItemDelegate = item;
        }

        /// <summary>
        /// Clear the pool items
        /// </summary>
        public void clearPool()
        {
            _items.Clear();
        }

        /// <summary>
        /// Get an item from the pool
        /// </summary>
        /// <returns></returns>
        public T getItem()
        {
            if (_items.Count > 0)
            {
                T item = _items[0];

                item.enabled = true;
                ((IPoolableObject)item).onReleasedFromPool();

                _items.RemoveAt(0);
                return item;
            }
            return createItem();
        }

        /// <summary>
        /// Add an item into the pool
        /// </summary>
        /// <param name="item"></param>
        public void storeItem(T item)
        {
            _items.Add(item);

            item.transform.position = new Vector2(-300, -300);
            item.enabled = false;

            ((IPoolableObject)item).onAddedToPool();
        }

        /// <summary>
        /// Create a new item using the delegate provided
        /// </summary>
        /// <returns></returns>
        private T createItem()
        {
            T item = _createItemDelegate.Invoke();
            return item;
        }
    }
}
