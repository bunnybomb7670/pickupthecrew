﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace PickUpTheCrew
{
    public static class VectorUtils
    {
        /// <summary>
        /// Rotate a vector by the angle supplied (in RADIANS)
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static Vector2 rotateVector(Vector2 vector, float angle)
        {
            return new Vector2(vector.X * (float)Math.Cos(angle) - vector.Y * (float)Math.Sin(angle), vector.Y * (float)Math.Cos(angle) - vector.X * (float)Math.Sin(angle));
        }

        /// <summary>
        /// Get the angle between two vectors in RADIANS?
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double angleBetweenVectors(Vector2 a, Vector2 b)
        {
            return Math.Atan2(b.Y - a.Y, b.X - a.X);
        }

        /// <summary>
        /// Get a position transformed from model position to world position
        /// </summary>
        /// <param name="point"></param>
        /// <param name="offset"></param>
        /// <param name="center"></param>
        /// <param name="scale"></param>
        /// <param name="rotation"></param>
        /// <returns></returns>
        public static Vector2 modelToWorldTransform(Vector2 point, GameObject obj)
        {
            Vector2 pos = VectorUtils.rotateVectorAroundPointWithOffset(obj.transform.position, point, obj.transform.rotation);
            return pos;
        }

        /// <summary>
        /// Return the magnitude of a Vector
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static double magnitude(Vector2 vector)
        {
            return (Math.Sqrt(vector.X + vector.Y));
        }

        /// <summary>
        /// Point a vector towards a target position
        /// </summary>
        /// <param name="position"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static float pointTowards(Vector2 position, Vector2 target)
        {
            Vector2 dir = position - target;
            return (float)Math.Atan2(-dir.X, dir.Y);
        }

        /// <summary>
        /// Set the length of a vector to the float value provided
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static Vector2 setLength(Vector2 vector, float length)
        {
            return Vector2.Normalize(vector) * length;
        }

        /// <summary>
        /// Calculate the perpendicular vector to the vector supplied
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public static Vector2 perp(Vector2 vector)
        {
            Vector2 v = new Vector2(-vector.Y, vector.X);
            v.Normalize();
            return v;
        }

        /// <summary>
        /// Calculate the DOT product of two vectors
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static float dot(Vector2 a, Vector2 b)
        {
            return a.X * b.X + a.Y * b.Y;
        }

        /// <summary>
        /// Limit the length of a vector to the float value provided
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static Vector2 truncate(Vector2 vector, float max)
        {
            if (vector.Length() > max)
            {
                return setLength(vector, max);
            }
            return vector;
        }

        /// <summary>
        /// Return an offset location rotated relative to the center point.
        /// </summary>
        /// <param name="center"></param>
        /// <param name="offset"></param>
        /// <param name="rotation"></param>
        /// <returns></returns>
        public static Vector2 rotateVectorAroundPointWithOffset(Vector2 center, Vector2 offset, float rotation)
        {
            return center + (Vector2.Transform(offset, Matrix.CreateRotationZ(rotation)));
        }
    }
}