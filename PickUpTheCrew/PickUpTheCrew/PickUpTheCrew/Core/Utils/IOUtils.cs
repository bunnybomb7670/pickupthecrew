﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Xml;
using System.IO;

namespace PickUpTheCrew
{
    public static class IOUtils
    {
        /// <summary>
        /// Read in a saved String and convert it to a Color
        /// </summary>
        /// <param name="inColor"></param>
        /// <returns></returns>
        public static Color parseColor(string inColor)
        {
            string[] co = inColor.Replace("{R:", "").Replace("G:", "").Replace("B:", "").Replace("A:", "").Replace("}", "").Split(' ');
            return new Color(int.Parse(co[0]), int.Parse(co[1]), int.Parse(co[2]), int.Parse(co[3]));
        }

        /// <summary>
        /// Read in a saved String and convert it to a Vector2
        /// </summary>
        /// <param name="inVector"></param>
        /// <returns></returns>
        public static Vector2 parseVector2(string inVector)
        {
            string[] ve = inVector.Replace("{X:", "").Replace("Y:", "").Replace("}", "").Split(' ');
            return new Vector2(float.Parse(ve[0]), float.Parse(ve[1]));
        }

        /// <summary>
        /// Create a new instance of a GameObject from the "class path" specified. This is the simplest way to factory initialize dynamically
        /// based on the input String provided to the method. Without this, it would be required to "register" each object somewhere to know
        /// how to re-initialize it afterwards, or through serialization
        /// </summary>
        /// <param name="game"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public static GameObject createGameObjectInstanceFromNode(MainGame game, XmlNode node)
        {
            Type type = Type.GetType(node.SelectSingleNode("Type").InnerText);
            GameObject obj = (GameObject)Activator.CreateInstance(type, game, node);
            return obj;
        }

        /// <summary>
        /// Create a new instance of a Renderer from the "class path" specified. 
        /// This will run the constructor which takes a MainGame and XmlNode parameter.
        /// </summary>
        /// <param name="game"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public static Renderer createRendererInstanceFromNode(GameObject gameObject, XmlNode node)
        {
            Type type = Type.GetType(node.SelectSingleNode("Type").InnerText);
            Renderer obj = (Renderer)Activator.CreateInstance(type, gameObject, node);
            return obj;
        }

        /// <summary>
        /// Check to see whether or not a saved game is stored, there are several outcomes with this, either the file does not exist,
        /// or that the file is empty ( a simple way to say that there is no save data ) without actually deleting files all the time
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool saveGameExists(string path)
        {
            if(checkForFile(path)) // Does the file exist?
            {
                return new FileInfo(path).Length > 0; // Does the file contain anything?
            }
            return false;
        }

        /// <summary>
        /// Check to see if a file exists at the path specified
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool checkForFile(string path)
        {
            return File.Exists(path);
        }
    }
}
