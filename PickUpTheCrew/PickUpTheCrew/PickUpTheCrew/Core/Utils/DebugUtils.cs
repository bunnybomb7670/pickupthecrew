﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Diagnostics;

namespace PickUpTheCrew
{
    public static class DebugUtils
    {
        /// <summary>
        /// Write a message to the Debug console (wrapper method)
        /// </summary>
        /// <param name="data"></param>
        public static void write(object data)
        {
            Debug.WriteLine(data.ToString());
        }

    }
}
