﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PickUpTheCrew
{
    public static class RandomUtils
    {
        private static Random _random = new Random();
        private static Random _levelRandom = new Random();

        /// <summary>
        /// The generic random used by the game
        /// </summary>
        public static Random random { get { return _random; } }

        /// <summary>
        /// The random used by level generation
        /// </summary>
        public static Random levelRandom { get { return _levelRandom; } }

        /// <summary>
        /// Initialize the random with a new seed
        /// </summary>
        /// <param name="seed"></param>
        public static void InitializeRandomWithSeed(int seed)
        {
            _random = new Random(seed);
        }

        /// <summary>
        /// Intitialize the level random with a new seed
        /// </summary>
        /// <param name="seed"></param>
        public static void resetLevelRandom(int seed)
        {
            _levelRandom = new Random(seed);
        }

        /// <summary>
        /// Get a random boolean value (roughly 1 in 2 chance)
        /// </summary>
        /// <returns></returns>
        public static bool randomBool()
        {
            return _random.Next(0, 10) > 5;
        }

        /// <summary>
        /// Get a random value between a range
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int range(int min, int max)
        {
            return _random.Next(min, max);
        }
    }
}