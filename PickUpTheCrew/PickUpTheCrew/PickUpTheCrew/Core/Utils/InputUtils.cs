﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PickUpTheCrew
{
    public class InputUtils
    {
        /// <summary>
        /// The last frames keyboard state (to allow differentiation and have single key press detection)
        /// </summary>
        private static KeyboardState lastKeyboardState;

        public static void updateState()
        {
            lastKeyboardState = Keyboard.GetState();
        }

        /// <summary>
        /// Catch a single key press for this key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool isKeyPressed(Keys key)
        {
            KeyboardState state = Keyboard.GetState();
            bool v = state.IsKeyDown(key) && lastKeyboardState.IsKeyUp(key);
            if (v)
            {
                lastKeyboardState = state;
            }

            return v;
        }

        /// <summary>
        /// Check if a key is down
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool isKeyDown(Keys key)
        {
            KeyboardState state = Keyboard.GetState();
            return state.IsKeyDown(key);
        }

        /// <summary>
        /// Check if a key is up
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool isKeyUp(Keys key)
        {
            KeyboardState state = Keyboard.GetState();
            return state.IsKeyUp(key);
        }

    }
}
