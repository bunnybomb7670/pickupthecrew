﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace PickUpTheCrew
{
    public class FPSCalculator
    {
        private int _frames = 0;
        private float _fps = 0.0f;

        private float _time;
        private TimeSpan _timeSample;
        private Stopwatch _stopWatch;

        /// <summary>
        /// The FPS calculation
        /// </summary>
        public float fps { get { return _fps; } }

        public FPSCalculator()
        {
            _timeSample = TimeSpan.FromSeconds(0.5);
            this._stopWatch = Stopwatch.StartNew();
        }

        /// <summary>
        /// A game update has happened, update the frame logic
        /// </summary>
        /// <param name="gameTime"></param>
        public void update(GameTime gameTime)
        {
            if (_stopWatch.Elapsed > _timeSample)
            {
                _fps = (float)_frames / (float)_stopWatch.Elapsed.TotalSeconds;

                _frames = 0;
                _stopWatch.Reset();
                _stopWatch.Start();

            }
        }

        /// <summary>
        /// A frame has happened, increment the frame counter
        /// </summary>
        public void frameUpdate()
        {
            _frames++;
        }

    }
}
