﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Xml;

/*
 The following websites were used as sources for gathering information on SAT:
 * 
 * http://www.sevenson.com.au/actionscript/sat/
 * http://www.metanetsoftware.com/technique/tutorialA.html#eberly
 * http://gamedevelopment.tutsplus.com/tutorials/collision-detection-using-the-separating-axis-theorem--gamedev-169
 * http://www.dyn4j.org/2010/01/sat/
 * 
 */

namespace PickUpTheCrew
{
    /// <summary>
    /// Structure to hold data about a Vector projection onto a 2D line
    /// </summary>
    public struct Projection
    {
        private float _min, _max;

        public Projection(float min, float max)
        {
            this._min = min;
            this._max = max;
        }

        /// <summary>
        /// Check for an overlap between two projections
        /// </summary>
        /// <param name="other"></param>
        /// <returns>Whether or not an overlap occured</returns>
        public bool overlap(Projection other)
        {
            return !(this._min > other._max || other._min > this._max);
        }

        /// <summary>
        /// Get the amount which two projections overlap
        /// </summary>
        /// <param name="other"></param>
        /// <returns>The overlap distance between two projections</returns>
        public float getOverlap(Projection other)
        {
            return Math.Min(this._max, other._max) - Math.Max(this._min, other._min); 
        }
    }

    public class Shape
    {
        private static float NORMAL_DRAW_LENGTH = 15.0f;
        private static Color NORMAL_DRAW_COLOR = Color.Cyan * 0.8f;
        private static Color EDGE_DRAW_COLOR = Color.Lime;
        private static Color EDGE_DRAW_COLOR_TRIGGER = Color.AliceBlue;

        private Vector2[] _points;

        /// <summary>
        /// An array containing all the vertices which make up this shape
        /// </summary>
        public Vector2[] points { get { return _points; } }

        public Shape(Vector2[] points)
        {
            _points = points;
        }

        /// <summary>
        /// Create the shape from a set of normalized points (-1 to 1) and scale them accordingly
        /// </summary>
        /// <param name="points"></param>
        /// <param name="scale"></param>
        public Shape(Vector2[] points, float scale)
        {
            for(int i = 0; i < points.Length; i++)
            {
                points[i] = new Vector2(points[i].X * scale, points[i].Y * scale);
            }
            _points = points;
        }

        /// <summary>
        /// Create a shape from another shape and scale it
        /// </summary>
        /// <param name="shape"></param>
        /// <param name="scale"></param>
        public Shape(Shape shape, float scale)
        {
            Vector2[] p = shape.points;
            Vector2[] nP = new Vector2[p.Length];

            for (int i = 0; i < nP.Length; i++)
            {
                nP[i] = new Vector2(p[i].X * scale, p[i].Y * scale);
            }

            _points = nP;
        }

        public Shape(XmlNode node)
        {
            XmlNodeList nodes = node.SelectNodes("Point");
            List<Vector2> p = new List<Vector2>();

            foreach(XmlNode n in nodes)
            {
                p.Add(IOUtils.parseVector2(n.InnerText));
            }

            _points = p.ToArray();
        }

        /// <summary>
        /// Get the bounds of the shape taking into account the position and rotation of the object which is passed
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>The bounds of the shape</returns>
        public Bounds getBounds(GameObject obj)
        {
            // Need to set the default values, these cannot just be initialized to 0 because
            // the transformed bounding area may not be anywhere near 0, set it to the first
            // vertex position
            Vector2 s = VectorUtils.modelToWorldTransform(_points[0], obj);

            float minX = s.X;
            float maxX = s.X;
            float minY = s.Y;
            float maxY = s.Y;

            for (int i = 0; i < _points.Length; i++)
            {
                Vector2 p = VectorUtils.modelToWorldTransform(_points[i], obj);
                
                if(p.X < minX)
                {
                    minX = p.X;
                }
                else if(p.X > maxX)
                {
                    maxX = p.X;
                }

                if (p.Y < minY)
                {
                    minY = p.Y;
                }
                else if (p.Y > maxY)
                {
                    maxY = p.Y;
                }
            }

            return new Bounds(minX, maxX, minY, maxY);
        }

        public void save(XmlWriter writer)
        {
            writer.WriteStartElement("Shape");

            writer.WriteStartElement("Points");

            for (int i = 0; i < _points.Length; i++)
            {
                writer.WriteElementString("Point", _points[i].ToString());
            }

            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        /// <summary>
        /// Draw the shapes physics info on the screen
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="renderManager"></param>
        /// <param name="colliding"></param>
        public void drawShape(PhysicsObject obj, RenderManager renderManager)
        {
            // Draw normals
            for (int i = 0; i < _points.Length; i++)
            {
                Vector2 startPos = VectorUtils.modelToWorldTransform(_points[i], obj);
                Vector2 endPos = VectorUtils.modelToWorldTransform(_points[i + 1 == _points.Length ? 0 : i + 1], obj);
                Vector2 mid = (endPos - startPos) / 2;

                Vector2 p = getNormalForLine(VectorUtils.modelToWorldTransform(_points[i], obj), VectorUtils.modelToWorldTransform(_points[i + 1 == _points.Length ? 0 : i + 1], obj));

                renderManager.drawLine(startPos + mid, startPos + mid + (p * NORMAL_DRAW_LENGTH), NORMAL_DRAW_COLOR, 1, 0.1f);
            }

            // Draw edges
            for (int i = 0; i < _points.Length; i++)
            {
                // temp new way of using the new position stuff
                Vector2 startPos = VectorUtils.modelToWorldTransform(_points[i], obj);
                Vector2 endPos = VectorUtils.modelToWorldTransform(_points[i + 1 == _points.Length ? 0 : i + 1], obj);
                renderManager.drawLine(startPos, endPos, obj.trigger ? EDGE_DRAW_COLOR_TRIGGER : EDGE_DRAW_COLOR, 1, 0.1f);
            }

            // Draw bounds
            renderManager.drawObjectBounds(getBounds(obj), obj);
        }

        /// <summary>
        /// Using SAT (Seperating Axis Theorem) Project points onto a line to determine whether the shapes collide. SAT works with any
        /// Convex shape to provide intersection checking. It makes use of projecting the "bounds" of a shape onto a line to work out
        /// whether or not they are colliding with each other.
        /// </summary>
        /// <param name="shapeObject"></param>
        /// <param name="hitObject"></param>
        /// <param name="forceVector"></param>
        /// <returns>Whether or not a collision occured</returns>
        public bool checkIntersection(PhysicsObject shapeObject, PhysicsObject hitObject, out Vector2 forceVector)
        {
            Shape hitShape = hitObject.shape;

            Vector2 smallestAxis = Vector2.Zero;
            float magnitude = 1000000;

            Vector2[] axes = getAllAxes(shapeObject);
            Vector2[] axes2 = hitShape.getAllAxes(hitObject);

            // Iterate all the first axes
            for (int i = 0; i < axes.Length; i++)
            {
                Vector2 axis = axes[i];
                Projection proj1 = createProjection(shapeObject, axis);
                Projection proj2 = hitShape.createProjection(hitObject, axis);

                // If there is no overlap, we know there cannot be a collision
                if (!proj1.overlap(proj2))
                {
                    forceVector = Vector2.Zero;
                    return false;
                }
                else // overlap occured, check for MTV
                {
                    float ov = proj1.getOverlap(proj2);
                    if (ov < magnitude)
                    {
                        magnitude = ov;
                        smallestAxis = axis;
                    }
                }
            }

            // Iterate all the second axes
            for (int i = 0; i < axes2.Length; i++)
            {
                Vector2 axis2 = axes2[i];

                Projection proj1 = createProjection(shapeObject, axis2);
                Projection proj2 = hitShape.createProjection(hitObject, axis2);

                // If there is no overlap, we know there cannot be a collision
                if (!proj1.overlap(proj2))
                {
                    forceVector = Vector2.Zero;
                    return false;
                }
                else // overlap occured, check for MTV
                {
                    float ov = proj1.getOverlap(proj2);
                    if (ov < magnitude)
                    {
                        magnitude = ov;
                        smallestAxis = axis2;
                    }
                }
            }

            // Check for inverted displacement..
            Vector2 diff = hitObject.transform.position - shapeObject.transform.position;
            float a = Vector2.Dot(smallestAxis, diff);
            if(a > 0)
            {
                smallestAxis *=-1;
            }

            // Calculate MTV ( Minimum Transfer Vector ) which is the required force 
            // to be able to force out of the bounds of the intersecting shape.
            smallestAxis *= magnitude;

            forceVector = smallestAxis;

            // We know that all axes have overlapped, we must have had a collision
            return true;
        }

        /// <summary>
        /// Create a projection ( min and max range ) onto a line
        /// </summary>
        /// <param name="axis"></param>
        /// <returns></returns>
        public Projection createProjection(PhysicsObject obj, Vector2 axis)
        {
            float min = VectorUtils.dot(VectorUtils.modelToWorldTransform(_points[0], obj), axis);
            float max = min;

            // Iterate all points and check whether they extend the min/max bounds on the projected line
            for (int i = 1; i < _points.Length; i++)
            {
                float v = VectorUtils.dot(VectorUtils.modelToWorldTransform(_points[i], obj), axis);
                if (v < min)
                {
                    min = v;
                }
                else if (v > max)
                {
                    max = v;
                }
            }
            return new Projection(min, max);
        }

        /// <summary>
        /// Get all projection axes for the shape
        /// </summary>
        /// <returns>All Axes fpr the shape</returns>
        public Vector2[] getAllAxes(PhysicsObject obj)
        {
            Vector2[] v = new Vector2[_points.Length];
            for (int i = 0; i < _points.Length; i++)
            {
                Vector2 a = VectorUtils.modelToWorldTransform(_points[i], obj);
                Vector2 b = VectorUtils.modelToWorldTransform(_points[i + 1 == points.Length ? 0 : i + 1], obj);
                Vector2 e = b - a;
                Vector2 p = VectorUtils.perp(e);
                v[i] = p;
            }
            return v;
        }

        /// <summary>
        /// Get a vector representing a normal perpendicular to the OUTSIDE of the vector expressed by the two points provided
        /// </summary>
        /// <param name="lineStart"></param>
        /// <param name="lineEnd"></param>
        /// <returns>The normal for the line specified</returns>
        private Vector2 getNormalForLine(Vector2 lineStart, Vector2 lineEnd)
        {
            Vector2 v = lineEnd - lineStart;
            v = new Vector2(-v.Y, v.X);
            v.Normalize();
            return v;
        }

    }
}