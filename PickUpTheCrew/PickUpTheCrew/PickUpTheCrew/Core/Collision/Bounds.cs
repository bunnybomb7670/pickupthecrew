﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace PickUpTheCrew
{
    public struct Bounds
    {
        private float _minX, _minY, _maxX, _maxY;

        public float minX { get { return _minX; } }
        public float maxX { get { return _maxX; } }
        public float minY { get { return _minY; } }
        public float maxY { get { return _maxY; } }

        public Bounds(float minX, float maxX, float minY, float maxY)
        {
            _minX = minX;
            _maxX = maxX;
            _minY = minY;
            _maxY = maxY;
        }

        /// <summary>
        /// Check for a bounds intersection by comparing min and max values
        /// </summary>
        /// <param name="other"></param>
        /// <returns>Whether or not the bounds intersect</returns>
        public bool intersects(Bounds other)
        {
            return !((_maxX <= other.minX || _minX >= other.maxX) || (_maxY <= other.minY || _minY >= other.maxY));
        }
    }
}
