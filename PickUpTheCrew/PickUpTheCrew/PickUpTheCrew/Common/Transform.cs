﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System.Xml;

namespace PickUpTheCrew
{
    public class Transform
    {
        private GameObject _gameObject;
        private Vector2 _position;
        private float _scale;
        private float _rotation;

        // The value of 360 degrees in radians
        private static float RADIAN_360 = 6.28318531f;

        public GameObject gameObject { get { return _gameObject; } }
        public Vector2 position { get { return _position; } set { _position = value; } }
        public Vector2 forward { get { return VectorUtils.rotateVector(new Vector2(0, -1), _rotation); } }
        public Vector2 center { get { return new Vector2(_position.X + (_gameObject.renderer.texture.Width / 2), _position.Y + (_gameObject.renderer.texture.Height / 2)); } }
        public float scale { get { return _scale; } set { _scale = value; } }
        public float rotation { get { return _rotation; } set { _rotation = value; } }
        public float rotationDegrees { get { return MathHelper.ToDegrees(_rotation); } }

        public Transform(GameObject gameObject) : this(gameObject, Vector2.Zero, 1.0f, 0.0f) { }

        public Transform(GameObject gameObject, Vector2 position, float scale, float rotation)
        {
            _gameObject = gameObject;
            _position = position;
            _scale = scale;
            _rotation = rotation;
        }

        public Transform(GameObject gameObject, XmlNode node)
        {
            _gameObject = gameObject;
            _position = IOUtils.parseVector2(node.SelectSingleNode("Position").InnerText);
            _scale = float.Parse(node.SelectSingleNode("Scale").InnerText);
            _rotation = float.Parse(node.SelectSingleNode("Rotation").InnerText);
        }

        /// <summary>
        /// Save the Transform to an XML Writer
        /// </summary>
        /// <param name="writer"></param>
        public virtual void save(XmlWriter writer)
        {
            writer.WriteStartElement("Transform");

            writer.WriteElementString("Position", _position.ToString());
            writer.WriteElementString("Scale", _scale.ToString());
            writer.WriteElementString("Rotation", _rotation.ToString());

            writer.WriteEndElement();
        }

        /// <summary>
        /// Rotate by the specified angle in radians
        /// </summary>
        /// <param name="degrees"></param>
        public void rotate(float radians)
        {
            _rotation += radians;
        }

        /// <summary>
        /// Get the angle within the bounds of 0 - 360 degrees (0 - 6.28318531 radians) ( eg : 390 would return 30 )
        /// </summary>
        /// <param name="angle"></param>
        /// <returns></returns>
        float keepWithinAngleRange(float angle)
        {
            return angle > RADIAN_360 ? angle % RADIAN_360 : angle < 0.0f ? Math.Abs(angle) : angle;
        }
    }
}