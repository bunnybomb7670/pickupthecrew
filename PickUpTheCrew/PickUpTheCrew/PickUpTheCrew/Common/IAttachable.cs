﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace PickUpTheCrew
{
    public interface IAttachable
    {
        Vector2 getAnchor();
        Vector2 getTopAnchor();
        Vector2 getBottomAnchor();
    }
}
