﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PickUpTheCrew
{
    public enum Direction
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
}
