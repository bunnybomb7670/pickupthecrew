﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PickUpTheCrew
{
    public enum GameState
    {
        INITIALIZE, // Once per game : load data etc.
        DEMO, // When game is left idle / between games, show sample gameplay etc.
        PLAY, // When game is playing
        TRANSITION // When the game has ended but demo has not yet started, this is used for displaying highscores and 
    }
}
