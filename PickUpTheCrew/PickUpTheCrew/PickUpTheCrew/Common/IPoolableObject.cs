﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PickUpTheCrew
{
    public interface IPoolableObject
    {
        void onAddedToPool();
        void onReleasedFromPool();
    }
}
